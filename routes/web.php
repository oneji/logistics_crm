<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Site routes
Route::middleware('web')->namespace('Site')->group(function() {
    Route::get('/', 'HomeController@index')->name('site.home');
    Route::get('/about', 'AboutController@index')->name('site.about');
    Route::get('/contact', 'ContactController@index')->name('site.contact');

    Route::get('/services', 'ServicesController@index')->name('site.services');
    Route::get('/services/overland', 'ServicesController@overland')->name('site.services.overland');
    Route::get('/services/airfreight', 'ServicesController@airfreight')->name('site.services.airfreight');
    Route::get('/services/multimodal', 'ServicesController@multimodal')->name('site.services.multimodal');
    Route::get('/services/rails', 'ServicesController@rails')->name('site.services.rails');
    Route::get('/kp/download', 'ServicesController@downloadKP')->name('site.kp.download');
});


// CRM routes
Auth::routes();

Route::get('/admin', 'Crm\HomeController@index')->name('crm.dashboard');
Route::middleware('auth')->prefix('admin')->namespace('Crm')->group(function() {
    Route::put('password/change', 'UserController@changePassword')->name('password.change');
    Route::resource('employees', 'EmployeeController');

    Route::resource('users', 'UserController');
    Route::post('users/status', 'UserController@changeStatus')->name('users.status');

    Route::get('acl', 'AclController@index')->name('acl.index');
    Route::post('permissions', 'PermissionController@store')->name('permission.store');
    Route::post('roles', 'RoleController@store')->name('role.store');
    Route::put('roles', 'RoleController@update')->name('role.update');

    Route::middleware('role:superadministrator|administrator|accountant')->group(function() {        
        Route::get('requests/overview', 'RequestController@overview')->name('requests.overview');
    });    

    Route::middleware('role:superadministrator|administrator|expeditor')->group(function() {
        Route::resource('requests', 'RequestController', [
            'except' => [ 'show' ]
        ]);  
        Route::get('requests/download/{filename}', 'RequestController@download')->name('requests.download');        
        Route::get('requests/preview', 'RequestController@previewDoc')->name('requests.preview');
        Route::get('requests/total-amount', 'RequestController@totalAmount')->name('requests.total-amount');
    });
});
