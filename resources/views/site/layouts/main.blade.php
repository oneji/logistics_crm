<!DOCTYPE html>
<html>
<head>
    <title>{{ isset($title) ? $title : 'CUBE' }} | Логистическая компания</title>
    <meta name="description" content="CUBE - лучшая логистическая компания в Душанбе">
    <meta name="author" content="Kamilov Timur">
    <meta name="keywords" content="transportation, logistics, cargo, business, cube, транспорт, транспортная компания, CUBE">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{ asset('site/css/bootstrap.css') }}" />
    <link rel="stylesheet" href="{{ asset('site/css/animate.css') }}" />
    <link rel="stylesheet" href="{{ asset('site/css/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('site/css/color-orange.css') }}" />
    <link rel="stylesheet" href="{{ asset('site/css/retina.css') }}" />
    <link rel="stylesheet" href="{{ asset('site/css/responsive.css') }}" />
    @yield('stylesheets')
    <!-- Google Web fonts -->
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800' />
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,800,700,600' />

    <!-- Font icons -->
    <link rel="stylesheet" href="{{ asset('site/icon-fonts/font-awesome-4.3.0/css/font-awesome.min.css') }}" />
</head>
<body>
    @include('site.partials._header')

    @yield('content')

    @include('site.partials._footer')
    <script src="{{ asset('site/js/jquery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('site/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('site/js/jquery.srcipts.min.js') }}"></script>
    <script src="{{ asset('site/owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('site/masterslider/masterslider.min.js') }}"></script>
    <script src="{{ asset('site/js/jquery.matchHeight-min.js') }}"></script>
    <script src="{{ asset('site/js/jquery.dlmenu.min.js') }}"></script>
    <script src="{{ asset('site/js/include.js') }}"></script>
    @yield('scripts')
</body>
</html>