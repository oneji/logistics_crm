<aside class="col-md-3 aside aside-left">
    <ul class="aside-widgets">
        <li class="widget widget_nav_menu clearfix">
            <div class="title">
                <h3>Услуги</h3>
            </div>
            <ul class="menu">
                <li class="menu-item {{ Request::is('services/overland') ? 'current-menu-item' : '' }}">
                    <a href="{{ route('site.services.overland') }}">Автомобильные грузоперевозки</a>
                </li>
                <li class="menu-item {{ Request::is('services/airfreight') ? 'current-menu-item' : '' }}">
                    <a href="{{ route('site.services.airfreight') }}">Авиаперевозки</a>
                </li>
                <li class="menu-item {{ Request::is('services/multimodal') ? 'current-menu-item' : '' }}">
                    <a href="{{ route('site.services.multimodal') }}">Мультимодальные перевозки</a>
                </li>
                <li class="menu-item {{ Request::is('services/rails') ? 'current-menu-item' : '' }}">
                    <a href="{{ route('site.services.rails') }}">Железножорожные перевозки</a>
                </li>
            </ul>
        </li>

        <li class="widget widget-text">
            <div class="title">
                <h3>Коммерческое предложение</h3>
            </div>

            <a href="{{ route('site.kp.download') }}" class="download-link">
                <span>
                    <i class="fa fa-file-pdf-o"></i>
                    Скачать КП
                </span>
            </a>
        </li>

        <li class="widget widget-text">
            <div class="title">
                <h3>Свяжитесь с нами</h3>
            </div>

            <img src="{{ asset('site/img/pics/locations.jpg') }}" alt="contact us"/>

            <br />

            <p>
                Свяжитесь с нами прямо сейчас.
            </p>

            <a href="{{ route('site.contact') }}" class="read-more">
                <span>
                    Наши контакты
                    <i class="fa fa-chevron-right"></i>
                </span>
            </a>
        </li>
    </ul>
</aside>