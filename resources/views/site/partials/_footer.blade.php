<div id="footer-wrapper" class="footer-dark">
    <footer id="footer">
        <div class="container">
            <div class="row">
                {{-- <ul class="col-md-3 col-sm-6 footer-widget-container clearfix">
                    <li class="widget widget_newsletterwidget">
                        <div class="title">
                            <h3>newsletter subscribe</h3>
                        </div>
                        <p>
                            Subscribe to our newsletter and we will 
                            inform you about newest projects and promotions.
                        </p>
                        <br />
                        <form class="newsletter">
                            <input class="email" type="email" placeholder="Your email...">
                            <input type="submit" class="submit" value="">
                        </form>
                    </li>
                </ul> --}}

                <ul class="col-md-4 col-sm-6 footer-widget-container">
                    <li class="widget widget_pages">
                        <div class="title">
                            <h3>Быстрые ссылки</h3>
                        </div>
                        <ul>
                            <li><a href="{{ route('site.about') }}">О нас</a></li>
                            <li><a href="{{ route('site.services') }}">Наши услуги</a></li>
                            <li><a href="#">Новости</a></li>
                            <li><a href="{{ route('site.contact') }}">Контакты</a></li>
                        </ul>
                    </li>
                </ul>

                <ul class="col-md-4 col-sm-6 footer-widget-container">
                    <li class="widget widget_pages">
                        <div class="title">
                            <h3>Наши предложения</h3>
                        </div>
                        <ul>
                            <li><a href="{{ route('site.services.overland') }}">Автомобильные грузоперевозки</a></li>
                            <li><a href="{{ route('site.services.airfreight') }}">Авиаперевозки</a></li>
                            <li><a href="{{ route('site.services.multimodal') }}">Мультимодальные перевозки</a></li>
                            <li><a href="{{ route('site.services.rails') }}">Железнодорожные перевозки</a></li>
                        </ul>
                    </li>
                </ul>

                <ul class="col-md-4 col-sm-6 footer-widget-container">
                    <li class="widget widget-text">
                        <div class="title">
                            <h3>Наши контакты</h3>
                        </div>
                        <address>
                            ул. Шерализода 91/2, Душанбе, Таджикистан
                        </address>
                        <span class="text-big">
                            (+992) 918 35 35 25
                        </span>
                        <br />
                        <a href="mailto:office@cubeml.com">office@cubeml.com</a>
                        <br />
                        <ul class="footer-social-icons">
                            <li><a href="#" class="fa fa-facebook"></a></li>
                            {{-- <li><a href="#" class="fa fa-twitter"></a></li>
                            <li><a href="#" class="fa fa-google-plus"></a></li> --}}
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </footer>

    <div class="copyright-container">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p>CUBE 2018. Все права защищены.</p>
                </div>
                {{-- <div class="col-md-6">
                    <p class="align-right">DESIGNED AND DEVELOPED BY <a href="www.pixel-industry.com">PIXEL INDUSTRY.</a> ELITE AUTHOR.</p>
                </div> --}}
            </div>
        </div>
    </div>

    <a href="#" class="scroll-up">Scroll</a>
</div>