<div class="header-wrapper header-transparent">
    <!-- .header.header-style01 start -->
    <header id="header"  class="header-style01">
        <!-- .container start -->
        <div class="container">
            <!-- .main-nav start -->
            <div class="main-nav">
                <!-- .row start -->
                <div class="row">
                    <div class="col-md-12">
                        <nav class="navbar navbar-default nav-left" role="navigation">

                            <!-- .navbar-header start -->
                            <div class="navbar-header">
                                <div class="logo">
                                    <a href="{{ route('site.home') }}">
                                        <img src="{{ asset('site/img/logo.png') }}" alt="CUBE - логистическая компания в г. Душанбе"/>
                                    </a>
                                </div>
                            </div>
                            <!-- .navbar-header start -->

                            <!-- MAIN NAVIGATION -->
                            <div class="collapse navbar-collapse">
                                <ul class="nav navbar-nav">
                                        <li class="{{ Request::is('/') ? 'current-menu-item' : '' }}"><a href="{{ route('site.home') }}">Главная</a></li>
                                        <li class="{{ Request::is('about') ? 'current-menu-item' : '' }}"><a href="{{ route('site.about') }}">О нас</a></li>
                                        <li class="{{ Request::is('services/*') || Request::is('services') ? 'current-menu-item' : '' }}"><a href="{{ route('site.services') }}">Услуги</a></li>
                                        <li><a href="#">Новости</a></li>
                                        <li class="{{ Request::is('contact') ? 'current-menu-item' : '' }}"><a href="{{ route('site.contact') }}">Контакты</a></li>
                                </ul>
                                <!-- RESPONSIVE MENU -->
                                <div id="dl-menu" class="dl-menuwrapper">
                                    <button class="dl-trigger">Меню</button>

                                    <ul class="dl-menu">
                                        <li class="{{ Request::is('/') ? 'current-menu-item' : '' }}"><a href="{{ route('site.home') }}">Главная</a></li>
                                        <li class="{{ Request::is('about') ? 'current-menu-item' : '' }}"><a href="{{ route('site.about') }}">О нас</a></li>
                                        <li class="{{ Request::is('services/*') || Request::is('services') ? 'current-menu-item' : '' }}"><a href="{{ route('site.services') }}">Услуги</a></li>
                                        <li><a href="#">Новости</a></li>
                                        <li class="{{ Request::is('contact') ? 'current-menu-item' : '' }}"><a href="{{ route('site.contact') }}">Контакты</a></li>
                                    </ul><!-- .dl-menu end -->
                                </div><!-- #dl-menu end -->
                            </div><!-- MAIN NAVIGATION END -->
                        </nav><!-- .navbar.navbar-default end -->
                    </div><!-- .col-md-12 end -->
                </div><!-- .row end -->
            </div><!-- .main-nav end -->
        </div><!-- .container end -->
    </header><!-- .header.header-style01 -->
</div><!-- .header-wrapper end -->