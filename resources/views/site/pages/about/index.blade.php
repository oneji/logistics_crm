@extends('site.layouts.main', [ 'title' => 'О нас' ])

@section('stylesheets')
    
@endsection

@section('content')
    <!-- .page-title start -->
    <div class="page-title-style01 page-title-negative-top pt-bkg02">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>CUBE - логистическая компания</h1>

                    <div class="breadcrumb-container">
                        <ul class="breadcrumb clearfix">
                            <li>Вы здесь:</li>
                            <li><a href="{{ route('site.home') }}">Главная</a></li>
                            <li><a href="{{ route('site.about') }}">О нас</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .page-title end -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="custom-heading">
                        <h2>О компании</h2>
                    </div><!-- .custom-heading end -->

                    <p>
                        ООО Компания "КУБ" - молодая перспективная команда высококвалифицированных 
                        специалистов, зарекомендовавших себя на рынке грузоперевозок, как надежный,
                        оперативный, высококачественный и добросовестный партнер, с индивидуальным
                        и гибким подходом к каждому заказчику, имеет честь предложить Вам свои услуги
                        на самых выгодных для Вас и Вашей компании условиях. 
                    </p>

                    <p>
                        Мы оказываем услуги по Таджикистану, странам СНГ, ближнему и дальнему 
                        зарубежью, авто, авиа и железнодорожными путями, как для больших компаний так 
                        и для маленьких фирм.
                    </p>
                </div><!-- .col-md-6 end -->

                <div class="col-md-6 animated triggerAnimation" data-animate="zoomIn">
                    <img src="{{ asset('site/img/pics/img25.jpg') }}" alt=""/>
                </div><!-- .col-md-6 end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div><!-- .page-content end -->  

    <div class="page-content custom-bkg bkg-light-blue mb-70">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="custom-heading">
                        <h2>Наша миссия</h2>
                    </div><!-- .custom-heading end -->

                    <p>
                        Наша компания постоянно стремится у улучшению сервиса и качества работы,
                        повышению профессиональных навыков команды и разработки самых выгодных и 
                        лояльных условий для своих действующих и потенциальных заказчиков, с целью
                        предоставления услуш наивысшего качества.
                    </p>
                </div><!-- .col-md-6 end -->

                <div class="col-md-6">
                    <div class="custom-heading">
                        <h2>Мы обещаем</h2>
                    </div><!-- .custom-heading end -->

                    <ul class="fa-ul">
                        <li>
                            <i class="fa fa-li fa-long-arrow-right"></i>
                            Предоставление услуг высшего качества перевозок всем нашим заказчикам.
                        </li>

                        <li>
                            <i class="fa fa-li fa-long-arrow-right"></i>
                            Вкладывать в наших сотрудников чтобы предоставить лучший сервис.
                        </li>

                        <li>
                            <i class="fa fa-li fa-long-arrow-right"></i>
                            Заботиться об окружении в соответствии с последними стандартами.
                        </li>

                        <li>
                            <i class="fa fa-li fa-long-arrow-right"></i>
                            Безопасность как высший приоритет для обеспечения безопасности всех процедур.
                        </li>
                    </ul><!-- .fa-ul end -->
                </div><!-- .col-md-6 end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div><!-- .page-content.custom-bkg end -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="custom-heading02">
                    <h2>Наши преимущества</h2>
                    <p>То, что отличает нас от других</p>
                </div>
            </div><!-- .row end -->

            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="service-icon-center">
                        <div class="icon-container">
                            <i class="fa fa-graduation-cap"></i>
                        </div>

                        <h4>Всегда обучаемся</h4>

                        <p>
                            Обучение персонала нашей компании никогда
                            не останавливается. Изучая новые технологии
                            и стандарты мы становимся лучше.  
                        </p>
                    </div><!-- .service-icon-center end -->
                </div><!-- .col-md-4 end -->

                <div class="col-md-4 col-sm-6">
                    <div class="service-icon-center">
                        <div class="icon-container">
                            <i class="fa fa-cogs"></i>
                        </div>

                        <h4>Последние технологии</h4>

                        <p>
                            Используем на практике только последние и лучшие технологии. 
                        </p>
                    </div><!-- .service-icon-center end -->
                </div><!-- .col-md-4 end -->

                <div class="col-md-4 col-sm-6">
                    <div class="service-icon-center">
                        <div class="icon-container">
                            <i class="fa fa-cubes"></i>
                        </div>

                        <h4>Безопасность и качество</h4>

                        <p>
                            Безопасность и качество всегда стоит у нас 
                            на первом место и неизменно прогрессирует.  
                        </p>
                    </div><!-- .service-icon-center end -->
                </div><!-- .col-md-4 end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div><!-- .page-content end -->

    <div class="page-content parallax parallax01 dark">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="call-to-action clearfix">
                        <div class="text">
                            <h2>Предоставление услуг логистики высшего уровня повсеместно.</h2>
                            <p>
                                Оставьте заявку на заказ, наши сотрудники с Вами свяжутся и вы 
                                сами убедитесь в качестве предоставляемого сервиса.
                            </p>                              
                        </div><!-- .text end -->

                        <a href="#" class="btn btn-big">
                            <span>Оставить заявку</span>
                        </a>
                    </div><!-- .call-to-action end -->
                </div><!-- .col-md-12 end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div><!-- .page-content.parallax end -->
@endsection


@section('scripts')
    
@endsection