@extends('site.layouts.main')

@section('stylesheets')
    <link rel="stylesheet" href="{{ asset('site/masterslider/style/masterslider.css') }}" />
    <link rel="stylesheet" href="{{ asset('site/masterslider/skins/default/style.css') }}" />
    <link rel='stylesheet' href="{{ asset('site/owl-carousel/owl.carousel.css') }}" />
@endsection

@section('content')
    <div id="masterslider" class="master-slider ms-skin-default mb-0">
        <!-- first slide -->
        <div class="ms-slide">
            <!-- slide background -->
            <img src="{{ asset('site/masterslider/blank.gif') }}') }}" data-src="{{ asset('site/img/slider/slide01.jpg') }}" alt="Strongest distribution network"/>  

            <img class="ms-layer" src="{{ asset('site/masterslider/blank.gif') }}" data-src="{{ asset('site/img/slider/slider-line.jpg') }}" alt=""
                style="left: 0; top: 310px;"
                data-type="image" 
                data-effect="left(short)" 
                data-duration="300"
                data-hide-effect="fade" 
                data-delay="0" 
                />

            <h3 class="ms-layer pi-caption01" 
                style="left: 0; top: 340px;" 
                data-type="text" 
                data-effect="left(short)" 
                data-duration="300"
                data-hide-effect="fade" 
                data-delay="300" 
                >
                Лучшие
            </h3>

            <h3 class="ms-layer pi-caption01" 
                style="left: 0; top: 400px;" 
                data-type="text"
                data-effect="left(short)" 
                data-duration="300"
                data-hide-effect="fade" 
                data-delay="600" 
                >
                транспортные
            </h3>

            <h3 class="ms-layer pi-caption01" 
                style="left: 0; top: 460px;" 
                data-type="text"
                data-effect="left(short)" 
                data-duration="300"
                data-hide-effect="fade" 
                data-delay="900" 
                >
                услуги
            </h3>
        </div><!-- .ms-slide end -->

        <!-- slide 02 start -->
        <div class="ms-slide">
            <!-- slide background -->
            <img src="{{ asset('site/masterslider/blank.gif') }}') }}" data-src="{{ asset('site/img/slider/slide02.jpg') }}" alt="Международные авиа перевозки"/> 

            <h3 class="ms-layer pi-caption03" 
                style="left: 120px; top: 390px;" 
                data-type="text" 
                data-effect="top(short)" 
                data-duration="300"
                data-hide-effect="fade" 
                data-delay="0" 
                >
                Транспортировки на самолете
            </h3>

            <img class="ms-layer" src="{{ asset('site/masterslider/blank.gif') }}" data-src="{{ asset('site/img/slider/slider-line.jpg') }}" alt=""
                style="left: 540px; top: 450px;"
                data-type="image" 
                data-effect="bottom(short)" 
                data-duration="300"
                data-hide-effect="fade" 
                data-delay="300" 
                />

            <p class="ms-layer pi-text"
            style="left: 272px; top: 470px;"
            data-type="text" 
            data-effect="top(short)" 
            data-duration="300"
            data-hide-effect="fade" 
            data-delay="600"      
            >
                Скорость. Точный график поставок. Безопасность.
            </p>
        </div><!-- .ms-slide end -->

        <!-- slide 03 start -->
        <div class="ms-slide">
            <!-- slide background -->
            <img src="{{ asset('site/masterslider/blank.gif') }}" data-src="{{ asset('site/img/slider/slide03.jpg') }}" alt="Worldwide freight services"/> 

            <h2 class="ms-layer pi-caption03" 
                style="left: 110px; top: 390px; font-size: 44px;" 
                data-type="text" 
                data-effect="top(short)" 
                data-duration="300"
                data-hide-effect="fade" 
                data-delay="0" 
                >
                Мультимодальные грузоперевозки
            </h2>

            <img class="ms-layer" src="{{ asset('site/masterslider/blank.gif') }}" data-src="{{ asset('site/img/slider/slider-line.jpg') }}" alt=""
                style="left: 540px; top: 450px;"
                data-type="image" 
                data-effect="bottom(short)" 
                data-duration="300"
                data-hide-effect="fade" 
                data-delay="300" 
                />

            <p class="ms-layer pi-text"
            style="left: 300px; top: 470px;"
            data-type="text" 
            data-effect="top(short)" 
            data-duration="300"
            data-hide-effect="fade" 
            data-delay="600"      
            >
                Дорога, небо, поезда или море. Мы можем все!
            </p>
        </div><!-- .ms-slide slide03 end -->

        <!-- slide 04 start -->
        <div class="ms-slide">
            <!-- slide background -->
            <img src="{{ asset('site/masterslider/blank.gif') }}" data-src="{{ asset('site/img/slider/slide04.jpg') }}" alt="Worldwide freight services"/> 

            <h2 class="ms-layer pi-caption03" 
                style="left: 100px; top: 390px; font-size: 60px;" 
                data-type="text" 
                data-effect="top(short)" 
                data-duration="300"
                data-hide-effect="fade" 
                data-delay="00" 
                >
                Автомобильные перевозки
            </h2>

            <img class="ms-layer" src="{{ asset('site/masterslider/blank.gif') }}" data-src="{{ asset('site/img/slider/slider-line.jpg') }}" alt=""
                style="left: 540px; top: 450px;"
                data-type="image" 
                data-effect="bottom(short)" 
                data-duration="300"
                data-hide-effect="fade" 
                data-delay="300" 
                />

            <p class="ms-layer pi-text"
            style="left: 330px; top: 470px;"
            data-type="text" 
            data-effect="top(short)" 
            data-duration="300"
            data-hide-effect="fade" 
            data-delay="600"      
            >
                Лучшие и современные методы логистики.
            </p>
        </div><!-- .ms-slide slide04 end -->
    </div><!-- #masterslider end -->

    <div class="page-content parallax parallax01 mb-70">
        <div class="container">
            <div class="row services-negative-top">
                <div class="col-md-4 col-sm-4">
                    <div class="service-feature-box">
                        <div class="service-media">
                            <img src="{{ asset('site/img/pics/img01.jpg') }}" alt="Trucking"/>

                            <a href="{{ route('site.services.overland') }}" class="read-more02">
                                <span>
                                    Подробнее
                                    <i class="fa fa-chevron-right"></i>
                                </span>
                            </a>
                        </div><!-- .service-media end -->

                        <div class="service-body">
                            <div class="custom-heading">
                                <h4>Автомобильные перевозки</h4>
                            </div><!-- .custom-heading end -->

                            <p>
                                Мы предоставляем перевозки грузов автомобильным 
                                транспортом с использованием современных методов
                                логистики. Основной обязанностью нашей компании 
                                выступает доставка груза в полной сохранности.
                            </p>
                        </div><!-- .service-body end -->
                    </div><!-- .service-feature-box-end -->
                </div><!-- .col-md-4 end -->

                <div class="col-md-4 col-sm-4">
                    <div class="service-feature-box">
                        <div class="service-media">
                            <img src="{{ asset('site/img/pics/img02.jpg') }}" alt="Trucking"/>

                            <a href="{{ route('site.services.multimodal') }}" class="read-more02">
                                <span>
                                    Подробнее
                                    <i class="fa fa-chevron-right"></i>
                                </span>
                            </a>
                        </div><!-- .service-media end -->

                        <div class="service-body">
                            <div class="custom-heading">
                                <h4>Мультимодальные перевозки</h4>
                            </div><!-- .custom-heading end -->

                            <p>
                                Мультимодальная перевозка - транспортировка грузов
                                по договору одним перевозчиком с применением различных
                                видов транспорта.
                            </p>
                        </div><!-- .service-body end -->
                    </div><!-- .service-feature-box-end -->
                </div><!-- .col-md-4 end -->

                <div class="col-md-4 col-sm-4">
                    <div class="service-feature-box">
                        <div class="service-media">
                            <img src="{{ asset('site/img/pics/img03.jpg') }}" alt="Trucking"/>

                            <a href="{{ route('site.services.airfreight') }}" class="read-more02">
                                <span>
                                    Подробнее
                                    <i class="fa fa-chevron-right"></i>
                                </span>
                            </a>
                        </div><!-- .service-media end -->

                        <div class="service-body">
                            <div class="custom-heading">
                                <h4>Авиаперевозки</h4>
                            </div><!-- .custom-heading end -->

                            <p>
                                Мы предоставляем качественные транспортировки
                                на самолете, достоинством данного метода 
                                является скорость, точный график поставок, высокая безопасность.
                            </p>
                        </div><!-- .service-body end -->
                    </div><!-- .service-feature-box-end -->
                </div><!-- .col-md-4 end -->
            </div><!-- .row end -->

            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('site.services') }}" class="btn btn-big btn-yellow btn-centered">
                        <span>
                            Смотреть все
                        </span>
                    </a>
                </div><!-- .col-md-12 end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div><!-- .page-content end -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="custom-heading02">
                        <h2>Что мы предлагаем</h2>
                        <p></p>
                    </div><!-- .custom-heading02 end -->
                </div><!-- .col-md-12 end -->
            </div><!-- .row end -->

            <div class="row mb-30">
                <div class="col-md-6 col-sm-6">
                    <div class="service-icon-left-boxed">
                        <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                            <img src="{{ asset('site/img/svg/pi-checklist-2.svg') }}" alt="checklist icon"/>
                        </div><!-- .icon-container end -->

                        <div class="service-details">
                            <h3>Подготовка документов</h3>

                            <p>
                                Точное документальное подтверждение всех хозяйственных операций 
                                на всем протяжении логистической цепи – залог дисциплинированности 
                                и контролируемости процесса.
                            </p>
                        </div><!-- .service-details end -->
                    </div><!-- .service-icon-left-boxed end -->
                </div><!-- .col-md-6 end -->

                <div class="col-md-6 col-sm-6">
                    <div class="service-icon-left-boxed">
                        <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                            <img src="{{ asset('site/img/svg/pi-globe-5.svg') }}" alt="globe icon"/>
                        </div><!-- .icon-container end -->

                        <div class="service-details">
                            <h3>Различные виды перевозок</h3>

                            <p>
                                Компания "CUBE" предлагает лучший сервис. 
                                Выберите автомобильные перевозки, перевозки мультимодальные, 
                                а такэе авиа.
                            </p>
                        </div><!-- .service-details end -->
                    </div><!-- .service-icon-left-boxed end -->
                </div><!-- .col-md-6 end -->
            </div><!-- .row.mb-30 end -->

            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="service-icon-left-boxed">
                        <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                            <img src="{{ asset('site/img/svg/pi-forklift-truck-5.svg') }}" alt="forktruck icon"/>
                        </div><!-- .icon-container end -->

                        <div class="service-details">
                            <h3>Загрузка</h3>

                            <p>
                                В перечень транспортно-экспедиционных услуг входит также выполнение 
                                погрузо-разгрузочных работ специалистами Компании «CUBE», 
                                в том числе с привлечением для этих работ специальной техники.
                            </p>
                        </div><!-- .service-details end -->
                    </div><!-- .service-icon-left-boxed end -->
                </div><!-- .col-md-6 end -->

                <div class="col-md-6 col-sm-6">
                    <div class="service-icon-left-boxed">
                        <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                            <img src="{{ asset('site/img/svg/pi-touch-desktop.svg') }}" alt="touch icon"/>
                        </div><!-- .icon-container end -->

                        <div class="service-details">
                            <h3>Согласование маршрутов следования</h3>

                            <p>
                                Не знаете как выбрать маршрут и какой вид транспортировки? 
                                Свяжитесь с нами и специалисты нашей компании с радостью Вам помогут.
                            </p>
                        </div><!-- .service-details end -->
                    </div><!-- .service-icon-left-boxed end -->
                </div><!-- .col-md-6 end -->
            </div><!-- .row.mb-30 end -->
        </div><!-- .container end -->
    </div><!-- .page-content end -->

    <div class="page-content custom-bkg bkg-dark-blue column-img-bkg dark"> <!-- mb-70 removed -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 col-md-offset-2 custom-col-padding-both">
                    <div class="custom-heading">
                        <h3>Классификация перевозок автомобильным транспортом</h3>
                    </div><!-- .custom-heading end -->

                    <p>
                        Мы покрываем различный сегмент товаров, начиная от еды
                        и напитков до скоропортящихся, негабаритных и 
                        тяжеловесных грузов.
                    </p>

                    <ul class="service-list clearfix">
                        <li>
                            <div class="icon-container">
                                <img class="svg-white" src="{{ asset('site/img/svg/pi-cargo-box-2.svg') }}" alt="icon"/>                                    
                            </div><!-- .icon-container end -->

                            <p>Контейнерная перевозка</p>
                        </li>

                        <li>
                            <div class="icon-container">
                                <img class="svg-white" src="{{ asset('site/img/svg/pi-mark-energy.svg') }}" alt="icon"/>                                    
                            </div><!-- .icon-container end -->

                            <p>Нефтепродукты</p>
                        </li>

                        <li>
                            <div class="icon-container">
                                <img class="svg-white" src="{{ asset('site/img/svg/pi-food-beverage.svg') }}" alt="icon"/>                                    
                            </div><!-- .icon-container end -->

                            <p>Еда и напитки</p>
                        </li>

                        <li>
                            <div class="icon-container">
                                <img class="svg-white" src="{{ asset('site/img/svg/pi-cargo-beverage.svg') }}" alt="icon"/>                                    
                            </div><!-- .icon-container end -->

                            <p>Скоропортящиеся продукты</p>
                        </li>

                        <li>
                            <div class="icon-container">
                                <img class="svg-white" src="{{ asset('site/img/svg/pi-truck-8.svg') }}" alt="icon"/>                                    
                            </div><!-- .icon-container end -->

                            <p>Опасные грузы</p>
                        </li>
                    </ul><!-- .service-list end -->
                </div><!-- .col-md-6 end -->

                <div class="col-md-6 img-bkg01">
                    <div>&nbsp;</div>
                </div>
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div>

    {{-- <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="custom-heading">
                        <h3>company news</h3>
                    </div><!-- .custom-heading end -->

                    <ul class="pi-latest-posts clearfix">
                        <li>
                            <div class="post-media">
                                <img src="{{ asset('site/img/blog/latest01.jpg') }}" alt=""/>
                            </div><!-- .post-media end -->

                            <div class="post-details">
                                <div class="post-date">
                                    <p>
                                        <i class="fa fa-calendar"></i>
                                        MAY 15, 2015
                                    </p>
                                </div>

                                <a href="news-single.html">
                                    <h4>
                                        Trucking - Company of the Year 2014
                                    </h4>
                                </a>

                                <a href="news-single.html" class="read-more">
                                    <span>
                                        Read more
                                        <i class="fa fa-chevron-right"></i>
                                    </span>
                                </a>
                            </div><!-- .post-details end -->
                        </li>

                        <li>
                            <div class="post-media">
                                <img src="{{ asset('site/img/blog/latest02.jpg') }}" alt=""/>
                            </div><!-- .post-media end -->

                            <div class="post-details">
                                <div class="post-date">
                                    <p>
                                        <i class="fa fa-calendar"></i>
                                        MAY 15, 2015
                                    </p>
                                </div>

                                <a href="news-single.html">
                                    <h4>
                                        First quartal 2015 revenue report released
                                    </h4>
                                </a>

                                <a href="news-single.html" class="read-more">
                                    <span>
                                        Read more
                                        <i class="fa fa-chevron-right"></i>
                                    </span>
                                </a>
                            </div><!-- .post-details end -->
                        </li>
                    </ul><!-- .pi-latest-posts end -->
                </div><!-- .col-md-4 end -->

                <div class="col-md-4 col-sm-6">
                    <div class="custom-heading">
                        <h3>Отзывы наших клиентов</h3>
                    </div><!-- .custom-heading end -->

                    <div class="carousel-container">
                        <div id="testimonial-carousel" class="owl-carousel owl-carousel-navigation">
                            <div class="owl-item">
                                <div class="testimonial">
                                    <p>
                                        Best customer support and 
                                        response time I have ever seen... 
                                        not to mention a kick ass theme! 
                                        Great feeling from this purchase.
                                        Thank you Pixel Industry!
                                    </p>

                                    <div class="testimonial-author">
                                        <p>
                                            TRAVIS COPLAND, <br />
                                            Transport & Logistics Solutions
                                        </p>
                                    </div><!-- .testimonial-author end -->
                                </div><!-- .testimonial end -->
                            </div><!-- .owl-item end -->

                            <div class="owl-item">
                                <div class="testimonial">
                                    <p>
                                        Thanks for developing 
                                        products to help many people 
                                        to work as I do. Thank you! 
                                        Success for all! I did not 
                                        need to use the support, 
                                        this never happened to me. 
                                        Pocket Theme bought today 
                                        for another client, very 
                                        good too! Hug!
                                    </p>

                                    <div class="testimonial-author">
                                        <p>
                                            ADRIANOSP, <br />
                                            Themeforest member
                                        </p>
                                    </div><!-- .testimonial-author end -->
                                </div><!-- .testimonial end -->
                            </div><!-- .owl-item end -->

                            <div class="owl-item">
                                <div class="testimonial">
                                    <p>
                                        We have several sites now 
                                        built in Elvyre across 
                                        several servers and have had 
                                        almost zero issues. The 
                                        documentation is great and 
                                        the featureset is phenomenal. 
                                        It is the best looking 
                                        pro-level and fairly easy-to-implement 
                                        advanced theme we are currently 
                                        regularly using. The end product 
                                        is great and easy to use and 
                                        configure. Highly recommended...
                                    </p>

                                    <div class="testimonial-author">
                                        <p>
                                            DIGITALMARKETINGASSOCIATES, <br />
                                            Themeforest member
                                        </p>
                                    </div><!-- .testimonial-author end -->
                                </div><!-- .testimonial end -->
                            </div><!-- .owl-item end -->
                        </div><!-- #testimonial-carousel end -->
                    </div><!-- .carousel-container end -->
                </div><!-- .col-md-4 end -->

                <div class="col-md-4 col-sm-12 clearfix">
                    <div class="custom-heading">
                        <h3>our locations</h3>
                    </div><!-- .custom-heading end -->

                    <img src="{{ asset('site/img/pics/locations.jpg') }}" alt="locations illustration"/>

                    <br />

                    <p>
                        Trucking Co. covers over 150 locations all 
                        over the globe plus numerous logistic 
                        partner companies from different areas of 
                        supply chain.
                    </p>

                    <a href="locations.html" class="read-more">
                        <span>
                            View all locations
                            <i class="fa fa-chevron-right"></i>
                        </span>
                    </a>
                </div><!-- .col-md-4 end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div><!-- .page-content end --> --}}    
@endsection
     
@section('scripts')
    <script src="{{ asset('site/owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('site/masterslider/masterslider.min.js') }}"></script>
    <script src="{{ asset('site/js/jquery.matchHeight-min.js') }}"></script>
    <script>
        /* <![CDATA[ */
        jQuery(document).ready(function ($) {
            'use strict';

            function equalHeight() {
                $('.page-content.column-img-bkg *[class*="custom-col-padding"]').each(function () {
                    var maxHeight = $(this).outerHeight();
                    $('.page-content.column-img-bkg *[class*="img-bkg"]').height(maxHeight);
                });
            };
            
            $(document).ready(equalHeight);
            $(window).resize(equalHeight);

            // MASTER SLIDER START 
            var slider = new MasterSlider();
            slider.setup('masterslider', {
                width: 1140, // slider standard width
                height: 854, // slider standard height
                space: 0,
                speed: 50,
                layout: "fullwidth",
                centerControls: false,
                loop: true,
                autoplay: true
                        // more slider options goes here...
                        // check slider options section in documentation for more options.
            });
            // adds Arrows navigation control to the slider.
            slider.control('arrows');

            // CLIENTS CAROUSEL START 
            $('#client-carousel').owlCarousel({
                items: 6,
                loop: true,
                margin: 30,
                responsiveClass: true,
                mouseDrag: true,
                dots: false,
                responsive: {
                    0: {
                        items: 2,
                        nav: true,
                        loop: true,
                        autoplay: true,
                        autoplayTimeout: 3000,
                        autoplayHoverPause: true,
                        responsiveClass: true
                    },
                    600: {
                        items: 3,
                        nav: true,
                        loop: true,
                        autoplay: true,
                        autoplayTimeout: 3000,
                        autoplayHoverPause: true,
                        responsiveClass: true
                    },
                    1000: {
                        items: 6,
                        nav: true,
                        loop: true,
                        autoplay: true,
                        autoplayTimeout: 3000,
                        autoplayHoverPause: true,
                        responsiveClass: true,
                        mouseDrag: true
                    }
                }
            });

            // TESTIMONIAL CAROUSELS START
            $('#testimonial-carousel').owlCarousel({
                items: 1,
                loop: true,
                margin: 30,
                responsiveClass: true,
                mouseDrag: true,
                dots: false,
                autoheight: true,
                responsive: {
                    0: {
                        items: 1,
                        nav: true,
                        loop: true,
                        autoplay: true,
                        autoplayTimeout: 3000,
                        autoplayHoverPause: true,
                        responsiveClass: true,
                        autoHeight: true
                    },
                    600: {
                        items: 1,
                        nav: true,
                        loop: true,
                        autoplay: true,
                        autoplayTimeout: 3000,
                        autoplayHoverPause: true,
                        responsiveClass: true,
                        autoHeight: true
                    },
                    1000: {
                        items: 1,
                        nav: true,
                        loop: true,
                        autoplay: true,
                        autoplayTimeout: 3000,
                        autoplayHoverPause: true,
                        responsiveClass: true,
                        mouseDrag: true,
                        autoHeight: true
                    }
                }
            });
        });
        /* ]]> */
    </script>
@endsection
        

       
    </body>
</html>
