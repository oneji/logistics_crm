@extends('site.layouts.main', [ 'title' => 'Автомобильные грузоперевозки' ])

@section('content')
    <!-- .page-title start -->
    <div class="page-title-style01 page-title-negative-top pt-bkg04">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Наши услуги - Автомобильные грузоперевозки</h1>

                    <div class="breadcrumb-container">
                        <ul class="breadcrumb clearfix">
                            <li>Вы здесь:</li>
                            <li>
                                <a href="{{ route('site.home') }}">Главная</a>
                            </li>
                            <li>
                                <a href="{{ route('site.services') }}">Наши услуги</a>
                            </li>

                            <li>
                                <a href="{{ route('site.services.overland') }}">Автомобильные грузоперевозки</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-content">
        <div class="container">
            <div class="row">
                @include('site.partials._services_sidebar')

                <div class="col-md-9">
                    <img src="{{ asset('site/img/pics/img27.jpg') }}" alt=""/>

                    <br />

                    <div class="custom-heading">
                        <h2>Автомобильные грузоперевозки</h2>
                    </div>

                    <p>
                        Мы предоставляем перевозки грузов автомобильным транспортом 
                        с использованием современных методов логистики. Основной 
                        обязанностью нашей компании выступает доставка груза в 
                        полной сохранности.
                    </p>

                    <br />

                    <br />

                    <h3>Преимущества</h3>

                    <p>
                        Автомобильные грузоперевозки являются лучшим способом доставки любых материалов, 
                        оборудования, техники, мебели и других ценных вещей быстро и надежно. С помощью а
                        втотранспорта можно производить перевозку почти любых объектов, к тому же он 
                        достаточно мобилен, чтобы груз доставлять к любому месту назначения
                    </p>

                    <br />

                    <img class="float-right" width="360" src="{{ asset('site/img/pics/img25.jpg') }}" alt=""/>

                    <ul class="fa-ul">
                        <li>
                            <i class="fa fa-li fa-long-arrow-right"></i>
                            Возможность доставки объектов любой конфигурации
                        </li>

                        <li>
                            <i class="fa fa-li fa-long-arrow-right"></i>
                            Мобильность автотранспорта. Автоперевозки могут производиться с доставкой «до подъезда»
                        </li>

                        <li>
                            <i class="fa fa-li fa-long-arrow-right"></i>
                            Именно автотранспорт используется для перевозки мебели, различной техники и других вещей 
                            при офисных или квартирных переездах, поскольку только таким образом возможно быстро и 
                            аккуратно доставить всё с одного места в другое
                        </li>

                        <li>
                            <i class="fa fa-li fa-long-arrow-right"></i>
                            Безопасность и надежность
                        </li>

                        <li>
                            <i class="fa fa-li fa-long-arrow-right"></i>
                            Оптимальная стоимость доставки
                        </li>

                        <li>
                            <i class="fa fa-li fa-long-arrow-right"></i>
                            Опытные сотрудники – профессионалы, не первый год занимающиеся грузоперевозками
                        </li>
                    </ul><!-- .fa-ul end -->

                    <br />
                    <br />

                    <div class="custom-heading">
                        <h3>Покрытый сегмент</h3>
                    </div><!-- .custom-heading end -->

                    <ul class="service-list-big-icons clearfix">
                        <li>
                            <div class="icon-container">
                                <img src="{{ asset('site/img/svg/pi-cargo-box-2.svg') }}" alt="retail svg icon"/>
                            </div>

                            <h4>Контейнерная перевозка</h4>
                        </li>

                        <li>
                            <div class="icon-container">
                                <img src="{{ asset('site/img/svg/pi-food-beverage.svg') }}" alt="food and beverage svg icon"/>
                            </div>

                            <h4>Еда и напитки</h4>
                        </li>

                        <li>
                            <div class="icon-container">
                                <img src="{{ asset('site/img/svg/pi-mark-energy.svg') }}" alt="retail svg icon"/>
                            </div>

                            <h4>Нефтепродукты</h4>
                        </li>
                    </ul><!-- .service-list-big-icons end -->
                </div><!-- .col-md-9 end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div><!-- .page-content end -->  
@endsection