@extends('site.layouts.main', [ 'title' => 'Наши услуги' ])

@section('content')
    <!-- .page-title start -->
    <div class="page-title-style01 page-title-negative-top pt-bkg03">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>CUBE - логистическая компания</h1>

                    <div class="breadcrumb-container">
                        <ul class="breadcrumb clearfix">
                            <li>Вы здесь:</li>
                            <li><a href="{{ route('site.home') }}">Главная</a></li>
                            <li><a href="{{ route('site.services') }}">Наши услуги</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .page-title end -->

    <div class="page-content custom-bkg bkg-grey">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="custom-heading02">
                        <h2>Наши услуги</h2>
                        <p>Логистические услуги с исльзованием современных подходов</p>
                    </div><!-- .custom-heading02 end -->
                </div><!-- .col-md-12 end -->
            </div><!-- .row end -->

            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="service-feature-box">
                        <div class="service-media">
                            <img src="{{ asset('site/img/pics/img01.jpg') }}" alt="Trucking"/>

                            <a href="{{ route('site.services.overland') }}" class="read-more02">
                                <span>
                                    Подробнее
                                    <i class="fa fa-chevron-right"></i>
                                </span>
                            </a>
                        </div><!-- .service-media end -->

                        <div class="service-body">
                            <div class="custom-heading">
                                <h4>Автомобильные перевозки</h4>
                            </div><!-- .custom-heading end -->

                            <p>
                                Мы предоставляем перевозки грузов автомобильным транспортом 
                                с использованием современных методов логистики. 
                                Основной обязанностью нашей компании выступает доставка груза в полной сохранности.
                            </p>
                        </div><!-- .service-body end -->
                    </div><!-- .service-feature-box-end -->
                </div><!-- .col-md-4 end -->

                <div class="col-md-4 col-sm-4">
                    <div class="service-feature-box">
                        <div class="service-media">
                            <img src="{{ asset('site/img/pics/img02.jpg') }}" alt="Trucking"/>

                            <a href="{{ route('site.services.multimodal') }}" class="read-more02">
                                <span>
                                    Подробнее
                                    <i class="fa fa-chevron-right"></i>
                                </span>
                            </a>
                        </div><!-- .service-media end -->

                        <div class="service-body">
                            <div class="custom-heading">
                                <h4>Мультимодальные перевозки</h4>
                            </div><!-- .custom-heading end -->

                            <p>
                                Мультимодальная перевозка - транспортировка грузов по договору 
                                одним перевозчиком с применением различных видов транспорта. 
                            </p>
                        </div><!-- .service-body end -->
                    </div><!-- .service-feature-box-end -->
                </div><!-- .col-md-4 end -->

                <div class="col-md-4 col-sm-4">
                    <div class="service-feature-box">
                        <div class="service-media">
                            <img src="{{ asset('site/img/pics/img03.jpg') }}" alt="Trucking"/>

                            <a href="{{ route('site.services.airfreight') }}" class="read-more02">
                                <span>
                                    Подробнее
                                    <i class="fa fa-chevron-right"></i>
                                </span>
                            </a>
                        </div><!-- .service-media end -->

                        <div class="service-body">
                            <div class="custom-heading">
                                <h4>Авиаперевозки</h4>
                            </div><!-- .custom-heading end -->

                            <p>
                                Мы предоставляем качественные транспортировки на самолете, 
                                достоинством данного метода является скорость, точный 
                                график поставок, высокая безопасность.
                            </p>
                        </div><!-- .service-body end -->
                    </div><!-- .service-feature-box-end -->
                </div><!-- .col-md-4 end -->
            </div><!-- .row end -->

            <div class="row">
                <div class="col-md-4 col-sm-4 col-md-offset-2">
                    <div class="service-feature-box">
                        <div class="service-media">
                            <img src="{{ asset('site/img/pics/img13.jpg') }}" alt="Trucking"/>

                            <a href="{{ route('site.services.rails') }}" class="read-more02">
                                <span>
                                    Подробнее
                                    <i class="fa fa-chevron-right"></i>
                                </span>
                            </a>
                        </div><!-- .service-media end -->

                        <div class="service-body">
                            <div class="custom-heading">
                                <h4>Железнодорожные перевозки</h4>
                            </div><!-- .custom-heading end -->

                            <p>
                                Предоставление одного из самых популярных и надежных видов
                                транспортировок на самом высоком уровне безопасности. Обеспечение безупречной транспортировки 
                                всех видов грузов, включая тяжелые партии товаров, используя эффективные и максимальные
                                возможности грузоподъемности вагонов.
                            </p>
                        </div><!-- .service-body end -->
                    </div><!-- .service-feature-box-end -->
                </div><!-- .col-md-4 end -->

                <div class="col-md-4 col-sm-4">
                    <div class="service-feature-box">
                        <div class="service-media">
                            <img src="{{ asset('site/img/pics/img15.jpg') }}" alt="Trucking"/>

                            <a href="consulting-services.html" class="read-more02">
                                <span>
                                    Подробнее
                                    <i class="fa fa-chevron-right"></i>
                                </span>
                            </a>
                        </div><!-- .service-media end -->

                        <div class="service-body">
                            <div class="custom-heading">
                                <h4>Консультация</h4>
                            </div><!-- .custom-heading end -->

                            <p>
                                Предоставление полного цикла управления включая выгодные по цене, 
                                а также быстрые автомобильные перевозки. Вы можете объединять эти услуги
                                с другими видами услуг транспортировки.
                            </p>
                        </div><!-- .service-body end -->
                    </div><!-- .service-feature-box-end -->
                </div><!-- .col-md-4 end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div><!-- .page-content end -->       

    <div class="page-content parallax parallax02 dark">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="custom-heading02 simple">
                        <h2>Компания "CUBE"</h2>
                    </div><!-- .custom-heading02 end -->

                    <div class="statement">
                        <p>
                            Компания гарантирует качественную доставку в указанные заказчиком сроки.
                        </p>
                    </div>
                </div><!-- .col-md-12 end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div><!-- .page-content end -->
@endsection