@extends('site.layouts.main', [ 'title' => 'Мультимодальные перевозки' ])

@section('content')
    <!-- .page-title start -->
    <div class="page-title-style01 page-title-negative-top pt-bkg07">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Наши услуги - Мультимодальные перевозки</h1>

                    <div class="breadcrumb-container">
                        <ul class="breadcrumb clearfix">
                            <li>Вы здесь:</li>
                            <li>
                                <a href="{{ route('site.home') }}">Главная</a>
                            </li>
                            <li>
                                <a href="{{ route('site.services') }}">Наши услуги</a>
                            </li>

                            <li>
                                <a href="{{ route('site.services.multimodal') }}">Мультимодальные перевозки</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .page-title end -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                @include('site.partials._services_sidebar')

                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="custom-heading">
                                <h3>Мультимодальные перевозки</h3>
                            </div><!-- .custom-heading end -->

                            <p>
                                Мультимодальные перевозки иногда называют перевозками 
                                смешанными. Это перевозки, при которых определенный 
                                груз на основании одного договора доставляется несколькими 
                                видами транспорта.
                                
                                {{-- К основным признакам, отличающим мультимодальные 
                                перевозки от перевозок другого типа, следует отнести, 
                                прежде всего, то обстоятельство, что мультимодальная 
                                перевозка выполняется двумя и более видами транспорта.  --}}
                            </p>

                            <p>
                                Сочетание видов транспорта подбирается в зависимости 
                                от пожеланий клиента. К примеру, если груз нужно доставить
                                быстро, будет преобладать воздушный способ перевозки. 
                                Если же скорость доставки неважна, груз может транспортироваться 
                                морем или по железной дороге. Это будет медленнее, но и 
                                гораздо дешевле. Стоимость мультимодальной перевозки будет зависеть 
                                также от количества выделяемых отрезков пути. Чем больше их число, 
                                тем медленнее и экономичнее будет перевозка.
                            </p>
                        </div><!-- .col-md-12 end -->
                    </div><!-- .row end -->

                    <div class="row">
                        <div class="col-md-5">
                            <img src="{{ asset('site/img/pics/img31.jpg') }}" alt=""/>
                        </div><!-- .col-md-5 end -->

                        <div class="col-md-7">
                            <h3>Туда куда Вам необходимо!</h3>

                            <p>
                                В процессе выполнения мультимодальной перевозки 
                                логистическая компания, отвечающая за ее выполнение, 
                                может самостоятельно оптимизировать маршрут перевозки, 
                                выбрать места пересечения границ государств и снять с 
                                грузовладельца большую часть вопросов по оформлению 
                                различной разрешительной и товаросопроводительной документации, 
                                необходимой для беспрепятственного прохождения груза 
                                на всех этапах мультимодальной перевозки. 
                            </p>
                        </div><!-- .col-md-7 end -->
                    </div><!-- .row end -->

                    <div class="row">
                        <div class="col-md-8">
                            <h3>Международные перевозки!</h3>

                            <p>
                                Мультимодальные перевозки – это, как правило, 
                                перевозки на дальние расстояния. Часто это международные 
                                перевозки грузов.
                            </p>

                            <p>
                                Важным при мультимодальной транспортировке оказывается то, 
                                что ее осуществляет одна, а не несколько компаний. Благодаря 
                                этому существенно сокращается время, которое было бы потрачено 
                                на устранение всех технических «накладок». Уменьшается и общая 
                                стоимость услуги. Кроме того, ответственность за сохранность груза 
                                также несет всего одна компания.
                            </p>
                        </div><!-- .col-md-8 end -->

                        <div class="col-md-4">
                            <img src="{{ asset('site/img/pics/img29.jpg') }}" alt=""/>
                        </div><!-- .col-md-4 end -->
                    </div><!-- .row end -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="custom-heading">
                                <h3>Покрытый сегмент</h3>
                            </div>
        
                            <ul class="service-list-big-icons clearfix">
                                <li>
                                    <div class="icon-container">
                                        <img src="{{ asset('site/img/svg/pi-cargo-box-2.svg') }}" alt="retail svg icon"/>
                                    </div>
        
                                    <h4>Контейнерная перевозка</h4>
                                </li>
        
                                <li>
                                    <div class="icon-container">
                                        <img src="{{ asset('site/img/svg/pi-food-beverage.svg') }}" alt="food and beverage svg icon"/>
                                    </div>
        
                                    <h4>Еда и напитки</h4>
                                </li>
        
                                <li>
                                    <div class="icon-container">
                                        <img src="{{ asset('site/img/svg/pi-mark-energy.svg') }}" alt="retail svg icon"/>
                                    </div>
        
                                    <h4>Нефтепродукты</h4>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
