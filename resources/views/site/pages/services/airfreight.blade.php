@extends('site.layouts.main', [ 'title' => 'Авиаперевозки' ])

@section('content')
    <!-- .page-title start -->
    <div class="page-title-style01 page-title-negative-top pt-bkg05">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Наши услуги - Авиаперевозки</h1>

                    <div class="breadcrumb-container">
                        <ul class="breadcrumb clearfix">
                            <li>Вы здесь:</li>
                            <li>
                                <a href="{{ route('site.home') }}">Главная</a>
                            </li>
                            <li>
                                <a href="{{ route('site.services') }}">Наши услуги</a>
                            </li>

                            <li>
                                <a href="{{ route('site.services.airfreight') }}">Авиаперевозки</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-content">
        <div class="container">
            <div class="row">
                @include('site.partials._services_sidebar')

                <div class="col-md-9">
                    <img src="{{ asset('site/img/pics/img28.jpg') }}" alt=""/>

                    <br />

                    <div class="custom-heading">
                        <h2>Авиаперевозки</h2>
                    </div>

                    <p>
                        Авиационный транспорт раньше считали диковинкой, чем-то недоступным и 
                        непривычным для многих людей. Однако тенденции с годами меняются, и 
                        теперь воздушный транспорт используют не только для перевозки пассажиров, 
                        но и для доставки больших партий грузов на территории одной страны и за ее 
                        границы. За очень короткий срок авиационные перевозки грузов получили высокую 
                        популярность и признание, благодаря преимуществам по сравнению с автомобильными, 
                        морскими и железнодорожными перевозками грузов. 
                    </p>

                    <br /><br />


                    <div class="row">

                        <div class="col-md-12">
                            <h3>Преимущества</h3>

                            <p>
                                Авиаперевозки могут перевезти груз туда, где другие имеющиеся виды транспорта, 
                                не справляются.
                            </p>

                            <ul class="fa-ul">
                                <li>
                                    <i class="fa fa-li fa-long-arrow-right"></i>
                                    Для авиационных грузоперевозок характерна высокая скорость доставки
                                </li>

                                <li>
                                    <i class="fa fa-li fa-long-arrow-right"></i>
                                    Другим достоинством перевозки грузов самолетом является возможность 
                                    преодоления любых препятствий рельефного характера – гор, пустынь, морей, океанов, пропастей
                                </li>

                                <li>
                                    <i class="fa fa-li fa-long-arrow-right"></i>
                                    Авиационные грузоперевозки обладают надежностью, поскольку попытки порчи или хищения 
                                    транспортируемых ценных грузов на большой высоте над землей сводятся при таких перевозках к нулю
                                </li>

                                <li>
                                    <i class="fa fa-li fa-long-arrow-right"></i>
                                    Надежность доставки повышается с использованием массовых средств мониторинга, позволяющим 
                                    осуществлять проверку сохранности и состояния транспортируемых товаров в режиме времени-онлайн
                                </li>

                                <li>
                                    <i class="fa fa-li fa-long-arrow-right"></i>
                                    При выполнении воздушных перевозок нет необходимости прохождения таможенного контроля на каждой границе
                                </li>

                                <li>
                                    <i class="fa fa-li fa-long-arrow-right"></i>
                                    Авиационными грузоперевозками можно доставлять самые различные грузы, независимо от их габаритов и тяжести
                                </li>
                            </ul><!-- .fa-ul end -->
                        </div><!-- .col-md-6 end -->
                    </div><!-- .row end -->

                    <div class="custom-heading">
                        <h3>Покрытый сегмент</h3>
                    </div>

                    <ul class="service-list-big-icons clearfix">
                        <li>
                            <div class="icon-container">
                                <img src="{{ asset('site/img/svg/pi-food-beverage.svg') }}" alt="food and beverage svg icon"/>
                            </div>
                            <h4>Еда и напитки</h4>
                        </li>

                        <li>
                            <div class="icon-container">
                                <img src="{{ asset('site/img/svg/pi-mark-energy.svg') }}" alt="retail svg icon"/>
                            </div>
                            <h4>Нефтепродукты</h4>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>  
@endsection