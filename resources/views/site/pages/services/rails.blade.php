@extends('site.layouts.main', [ 'title' => 'Железнодорожные перевозки' ])

@section('content')
    <!-- .page-title start -->
    <div class="page-title-style01 page-title-negative-top pt-bkg14">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Наши услуги - Железнодорожные перевозки</h1>
                    <div class="breadcrumb-container">
                        <ul class="breadcrumb clearfix">
                            <li>Вы здесь:</li>
                            <li>
                                <a href="{{ route('site.home') }}">Главная</a>
                            </li>
                            <li>
                                <a href="{{ route('site.services') }}">Наши услуги</a>
                            </li>
                            <li>
                                <a href="{{ route('site.services.rails') }}">Железнодорожные перевозки</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .page-title start -->   
    <div class="page-content">
        <div class="container">
            <div class="row">
                @include('site.partials._services_sidebar')

                <div class="col-md-9">        
                    <img src="{{ asset('site/img/pics/img39.jpg') }}" alt=""/>
                    <br />
                    <div class="custom-heading">
                        <h2>Железнодорожные международные перевозки</h2>
                    </div>
                    <p>
                        Железнодорожные перевозки грузов предоставляют ряд 
                        преимуществ перед автомобильным, водным и воздушным 
                        транспортом: доступные цены, высокая надежность 
                        транспортных средств, а также безопасность во время 
                        перевозок. Именно данный способ выбирается заинтересованными 
                        лицами в подавляющем большинстве случаев, когда существует 
                        возможность использовать железнодорожное сообщение между 
                        пунктами назначения.
                    </p>

                    <br />

                    <div class="row">
                        <div class="col-md-5">
                            <img src="{{ asset('site/img/pics/img38.jpg') }}" alt=""/>
                        </div>

                        <div class="col-md-7">                                
                            <ul class="fa-ul large-icons">
                                <li>
                                    <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                                        <i class="fa fa-check-circle"></i>
                                    </div>
                                    <div class="li-content">
                                        <h3>Надежность ЖД перевозок</h3>
                                        <p>
                                            Контейнерные ЖД перевозки исключают порчу груза, 
                                            вызванную влиянием атмосферных воздействий. 
                                            Транспортируемый товар не страдает от прямого 
                                            солнечного света или различных механических воздействий. 
                                        </p>
                                    </div>
                                </li>

                                <li>
                                    <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                                        <i class="fa fa-check-circle"></i>
                                    </div>
                                    <div class="li-content">
                                        <h3>Перевозка негабаритных грузов</h3>
                                        <p>
                                            Само понятие «негабаритный груз» подразумевает 
                                            дополнительные требования к подвижному составу, 
                                            пропускной способности транспортных сетей и 
                                            накладываемым на них ограничений.
                                        </p>
                                    </div>
                                </li>

                                <li>
                                    <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                                        <i class="fa fa-check-circle"></i>
                                    </div>

                                    <div class="li-content">
                                        <h3>Транспортно-экспедиторские услуги</h3>

                                        <p>
                                            Процедура ЖД перевозки грузов обеспечивается 
                                            системой взаимосвязанных операций, которые 
                                            оказывают многие транспортные компании: 
                                            упаковка, маркировка, погрузка, выгрузка и прочие. 
                                        </p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="custom-heading">
                        <h3>Покрытый сегмент</h3>
                    </div>

                    <ul class="service-list-big-icons clearfix">
                        <li>
                            <div class="icon-container">
                                <img src="{{ asset('site/img/svg/pi-cargo-box-2.svg') }}" alt="retail svg icon"/>
                            </div>
                            <h4>Контейнерная перевозка</h4>
                        </li>                        
                        <li>
                            <div class="icon-container">
                                <img src="{{ asset('site/img/svg/pi-food-beverage.svg') }}" alt="food and beverage svg icon"/>
                            </div>
                            <h4>Еда и напитки</h4>
                        </li>
                        <li>
                            <div class="icon-container">
                                <img src="{{ asset('site/img/svg/pi-mark-energy.svg') }}" alt="retail svg icon"/>
                            </div>
                            <h4>Нефтепродукты</h4>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

@endsection