@extends('site.layouts.main', [ 'title' => 'О нас' ])

@section('stylesheets')
    
@endsection

@section('content')
    <!-- .page-title start -->
    <div class="page-title-style01 page-title-negative-top pt-bkg08">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Наши контакты</h1>
                    <div class="breadcrumb-container">
                        <ul class="breadcrumb clearfix">
                            <li>Вы здесь:</li>
                            <li>
                                <a href="{{ route('site.home') }}">Главная</a>
                            </li>
                            <li>
                                <a href="{{ route('site.contact') }}">Контакты</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .page-title end -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="custom-heading">
                        <h3>Заявка</h3>
                    </div>
                    <p>
                        Cozy sphinx waves quart jug of bad milk. A very bad 
                        quack might jinx zippy fowls. Few quips galvanized 
                        the mock jury box. Quick brown dogs jump over the 
                        lazy fox. The jay, pig, fox, zebra, and my wolves 
                        quack! Blowzy red vixens fight for a quick jump. 
                        Joaquin Phoenix was gazed by MTV for luck. A 
                        wizard’s job is to vex chumps quickly in fog. Watch 
                        "Jeopardy!", Alex Trebek's fun TV quiz game. 
                        Woven silk pyjamas exchanged for blue quartz. 
                        Brawny gods just.
                    </p>
                    <br />
                    
                    <form class="wpcf7 clearfix">
                        <fieldset>
                            <label>
                                <span class="required">*</span> Your request:
                            </label>

                            <select class="wpcf7-form-control-wrap wpcf7-select" id="contact-inquiry">
                                <option value="I need an offer for contract logistics">I need an offer for contract logistics</option>
                                <option value="I need an offer for air freight">I need an offer for air freight</option>
                                <option value="I want to become your partner">I want to become your partner</option>
                                <option value="I have some other request">I have some other request</option>
                            </select>
                        </fieldset>

                        <fieldset>
                            <label>
                                <span class="required">*</span> First Name:
                            </label>

                            <input type="text" class="wpcf7-text" id="contact-name">
                        </fieldset>

                        <fieldset>
                            <label>
                                <span class="required">*</span> Last Name:
                            </label>

                            <input type="text" class="wpcf7-text" id="contact-last-name">
                        </fieldset>

                        <fieldset>
                            <label>
                                <span class="required">*</span> Email:
                            </label>

                            <input type="url" class="wpcf7-text" id="contact-email">
                        </fieldset>

                        <fieldset>
                            <label>
                                <span class="required">*</span> Message:
                            </label>

                            <textarea rows="8" class="wpcf7-textarea" id="contact-message"></textarea>
                        </fieldset>

                        <input type="submit" class="wpcf7-submit" value="send" />
                    </form>
                </div>

                <div class="col-md-6">
                    <div class="custom-heading">
                        <h3>Наше местоположение</h3>
                    </div>

                    <div id="map"></div>

                    <div class="custom-heading">
                        <h4>Информация</h4>
                    </div>

                    <address>
                        ул. Шерализода 91/2, Душанбе, Таджикистан
                    </address>

                    <span class="text-big colored">
                        (+992) 918 35 35 25
                    </span>
                    <br />

                    <a href="mailto:office@cubeml.com">office@cubeml.com</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script> <!-- google maps -->
    <script>
        /* <![CDATA[ */
        jQuery(document).ready(function ($) {
            'use strict';
            // GOOGLE MAPS START
            window.marker = null;

            function initialize() {
                var map;

                var nottingham = new google.maps.LatLng(52.934658, -1.131450);

                var style = [
                    {"featureType": "road",
                        "elementType":
                                "labels.icon",
                        "stylers": [
                            {"saturation": 1},
                            {"gamma": 1},
                            {"visibility": "on"},
                            {"hue": "#e6ff00"}
                        ]
                    },
                    {"elementType": "geometry", "stylers": [
                            {"saturation": -85}
                        ]
                    }
                ];

                var mapOptions = {
                    // SET THE CENTER
                    center: nottingham,
                    // SET THE MAP STYLE & ZOOM LEVEL
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    zoom: 9,
                    // SET THE BACKGROUND COLOUR
                    backgroundColor: "#eeeeee",
                    // REMOVE ALL THE CONTROLS EXCEPT ZOOM
                    panControl: true,
                    zoomControl: true,
                    mapTypeControl: true,
                    scaleControl: true,
                    streetViewControl: true,
                    overviewMapControl: true,
                    scrollwheel: false,
                    zoomControlOptions: {
                        style: google.maps.ZoomControlStyle.SMALL
                    }

                };
                map = new google.maps.Map(document.getElementById('map'), mapOptions);

                // SET THE MAP TYPE
                var mapType = new google.maps.StyledMapType(style, {name: "Grayscale"});
                map.mapTypes.set('grey', mapType);
                map.setMapTypeId('grey');

                //CREATE A CUSTOM PIN ICON
                var marker_image = 'img/pin.png';
                var pinIcon = new google.maps.MarkerImage(marker_image, null, null, null, new google.maps.Size(21, 34));

                marker = new google.maps.Marker({
                    position: nottingham,
                    map: map,
                    icon: pinIcon,
                    title: 'Trucking Headquarters, Nothingham'
                });
            }

            google.maps.event.addDomListener(window, 'load', initialize);

            // CONTACT FORM AJAX SUBMIT START
            $('.wpcf7 .wpcf7-submit').on('click', function (event) {
                event.preventDefault();
                var inquiry = $('#contact-inquiry option:selected').text();
                var name = $('#contact-name').val();
                var lastname = $('#contact-last-name').val();
                var email = $('#contact-email').val();
                var contact_message = $('#contact-message').val();
                var form_data = new Array({'inquiry': inquiry, 'name': name, 'last_name': lastname, 'email': email, 'message': contact_message});
                $.ajax({
                    type: 'POST',
                    url: "contact.php",
                    data: ({'action': 'contact', 'form_data': form_data})
                }).done(function (data) {
                    alert(data);
                });
            }); // CONTACT FORM AJAX SUBMIT END

        });
        /* ]]> */
    </script>
@endsection