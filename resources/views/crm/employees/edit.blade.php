@extends('crm.layouts.main', [ 'title' => 'Сотрудник', 'employee_active' => 'current_section' ])

@section('content')
    {!! Form::open([ 'url' => route('employees.update', $employee['id']), 'method' => 'put', 'id' => 'employee_edit_form', 'class' => 'uk-form-stacked' ]) !!}    
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-large-7-10">
                <div class="md-card">
                    <div class="user_heading" data-uk-sticky="{ top: 48, media: 960 }">
                        <div class="user_heading_avatar fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail">
                                <img src="{{ asset('crm/img/avatars/user.png') }}" alt="user avatar"/>
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail"></div>
                            <div class="user_avatar_controls">
                                <span class="btn-file">
                                    <span class="fileinput-new"><i class="material-icons">&#xE2C6;</i></span>
                                    <span class="fileinput-exists"><i class="material-icons">&#xE86A;</i></span>
                                    <input type="file" name="user_edit_avatar_control" id="user_edit_avatar_control">
                                </span>
                                <a href="#" class="btn-file fileinput-exists" data-dismiss="fileinput"><i class="material-icons">&#xE5CD;</i></a>
                            </div>
                        </div>
                        <div class="user_heading_content">
                            <h2 class="heading_b"><span class="uk-text-truncate" id="user_edit_uname">{{ $employee['full_name'] }}</span><span class="sub-heading" id="user_edit_position">{{ $employee['position'] }}</span></h2>
                        </div>
                        <div class="md-fab-wrapper">
                            <div class="md-fab md-fab-toolbar md-fab-small md-fab-accent">
                                <i class="material-icons">&#xE8BE;</i>
                                <div class="md-fab-toolbar-actions">
                                    <button type="submit" id="user_edit_save" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}" title="Сохранить"><i class="material-icons md-color-white">&#xE161;</i></button>
                                    <button type="submit" id="user_edit_delete" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}" title="Удалить"><i class="material-icons md-color-white">&#xE872;</i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="user_content">
                        <ul id="user_edit_tabs" class="uk-tab" data-uk-tab="{connect:'#user_edit_tabs_content'}">
                            <li class="uk-active"><a href="#">Основное</a></li>
                        </ul>
                        <ul id="user_edit_tabs_content" class="uk-switcher uk-margin">
                            <li>
                                <div class="uk-margin-top">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2">
                                            {{ Form::label('full_name', 'ФИО') }}
                                            {{ Form::text('full_name', $employee['full_name'], [ 'class' => 'md-input' ]) }}
                                        </div>
                                        <div class="uk-width-medium-1-2">
                                            {{ Form::label('position', 'Должность') }}
                                            {{ Form::text('position', $employee['position'], [ 'class' => 'md-input' ]) }}    
                                        </div>
                                    </div>
                                    <h3 class="full_width_in_card heading_c">
                                        Контакты
                                    </h3>
                                    <div class="uk-grid">
                                        <div class="uk-width-1-1">
                                            <div class="uk-grid uk-grid-width-1-1 uk-grid-width-large-1-2" data-uk-grid-margin>
                                                <div>
                                                    <div class="uk-input-group">
                                                        <span class="uk-input-group-addon">
                                                            <i class="md-list-addon-icon material-icons">&#xE158;</i>
                                                        </span>
                                                        {{ Form::label('email', 'Email') }}
                                                        {{ Form::text('email', $employee['email'], [ 'class' => 'md-input' ]) }}    
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="uk-input-group">
                                                        <span class="uk-input-group-addon">
                                                            <i class="md-list-addon-icon material-icons">&#xE0CD;</i>
                                                        </span>
                                                        {{ Form::label('phone', 'Phone') }}
                                                        {{ Form::text('phone', $employee['phone'], [ 'class' => 'md-input' ]) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            {{--  <div class="uk-width-large-3-10">
                <div class="md-card">
                    <div class="md-card-content">
                        <h3 class="heading_c uk-margin-medium-bottom">Другие настройки</h3>
                        <div class="uk-form-row">
                            <input type="checkbox" checked data-switchery id="user_edit_active" />
                            <label for="user_edit_active" class="inline-label">Активен</label>
                        </div>
                        <hr class="md-hr">
                        <div class="uk-form-row">
                            <label class="uk-form-label" for="user_edit_role">Роль пользователя</label>
                            <select data-md-selectize>
                                <option value="">Выберите роль</option>
                                @foreach ($roles as $role)
                                    <option value="{{ $role['id'] }}" {{ Auth::user()->hasRole($role['name']) ? 'selected' : '' }}>{{ $role["display_name"] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>  --}}
        </div>
    {!! Form::close() !!}
@endsection

@section('scripts')
    <script src="{{ asset('crm/js/uikit_fileinput.min.js') }}"></script>
    <script src="{{ asset('crm/js/pages/employees/page_user_edit.min.js') }}"></script>
    <script src="{{ asset('crm/js/pages/employees/employees.js') }}"></script>
@endsection