@extends('crm.layouts.main', [ 'title' => 'Сотрудники', 'employee_active' => 'current_section' ])

@section('content')
    <h3 class="heading_b uk-margin-bottom">Список сотрудников</h3>
    {{--  Search bar  --}}
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-2">
                    <div class="uk-vertical-align">
                        <div class="uk-vertical-align-middle">
                            <ul id="contact_list_filter" class="uk-subnav uk-subnav-pill uk-margin-remove">
                                <li class="uk-active" data-uk-filter=""><a href="#">Все</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="uk-width-medium-1-2">
                    <label for="contact_list_search">Поиск... (мин 3 символа.)</label>
                    <input class="md-input" type="text" id="contact_list_search"/>
                </div>
            </div>
        </div>
    </div>
    {{--  Search bar  --}}
    <h3 class="heading_b uk-text-center grid_no_results" style="display:none">Не найдено...</h3>

    <div class="uk-grid-width-small-1-2 uk-grid-width-medium-1-3 uk-grid-width-large-1-4 uk-grid-width-xlarge-1-5 hierarchical_show" id="contact_list">
        @foreach ($employees as $employee) 
            <div data-uk-filter="goodwin-nienow, {{ Illuminate\Support\Str::lower($employee['full_name']) }}, {{ $employee['email'] }}">
                <div class="md-card md-card-hover">
                    <div class="md-card-head">
                        <div class="md-card-head-menu" data-uk-dropdown="{pos:'bottom-right'}">
                            <i class="md-icon material-icons">&#xE5D4;</i>
                            <div class="uk-dropdown uk-dropdown-small">
                                <ul class="uk-nav">
                                    @permission('update-employees')
                                        <li><a href="{{ route('employees.edit', $employee['id']) }}">Изменить</a></li>
                                    @endpermission
                                    @permission('delete-employees')
                                        <li><a href="{{ route('employees.destroy', $employee['id']) }}">Удалить</a></li>
                                    @endpermission
                                </ul>
                            </div>
                        </div>
                        <div class="uk-text-center">
                            <img class="md-card-head-avatar" src="{{ asset('crm/img/avatars/user.png') }}" alt="{{ $employee['full_name'] }}" />
                        </div>
                        <h3 class="md-card-head-text uk-text-center">
                            {{ $employee['full_name'] }}
                            <span class="uk-text-truncate">{{ $employee['position'] }}</span>
                        </h3>
                    </div>
                    <div class="md-card-content">
                        <ul class="md-list">
                            <li>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Email</span>
                                    <span class="uk-text-small uk-text-muted uk-text-truncate"><a href="mailto:{{ $employee['email'] }}" class="__cf_email__">{{ $employee['email'] }}</a></span>
                                </div>
                            </li>
                            <li>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Телефон</span>
                                    <span class="uk-text-small uk-text-muted">{{ $employee['phone'] }}</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('crm/js/pages/employees/page_contact_list.min.js') }}"></script>
@endsection