@extends('crm.layouts.main', [ 'home_active' => 'current_section' ])

@section('stylesheets')
    <style>
        .md-card__icon {
            font-size: 36px; 
            margin-top: 7px
        }
    </style>
@endsection

@section('content')
    <div class="uk-grid uk-grid-width-large-1-4 uk-grid-width-medium-1-2 uk-grid-medium ">
        <div class="uk-width-medium-1-4 uk-width-small-1-2">
            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-float-right">
                        <i class="material-icons md-card__icon">assignment</i>
                    </div>
                    <span class="uk-text-muted uk-text-small">Заявки</span>
                    <h2 class="uk-margin-remove"><span>{{ count($requests) }}</span></h2>
                </div>
            </div>
        </div>
        @permission('create-users|read-users|update-users|delete-users')
            <div class="uk-width-medium-1-4 uk-width-small-1-2">
                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-float-right">
                            <i class="material-icons md-card__icon">people</i>
                        </div>
                        <span class="uk-text-muted uk-text-small">Пользователи</span>
                        <h2 class="uk-margin-remove"><span>{{ count($users) }}</span></h2>
                    </div>
                </div>
            </div>
        @endpermission
        @permission('create-acl|read-acl|update-acl|delete-acl')
            <div class="uk-width-medium-1-4 uk-width-small-1-2">
                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-float-right">
                            <i class="material-icons md-card__icon">lock</i>
                        </div>
                        <span class="uk-text-muted uk-text-small">Роли</span>
                        <h2 class="uk-margin-remove"><span>{{ count($roles) }}</span></h2>
                    </div>
                </div>
            </div>
        @endpermission
    </div>
@endsection