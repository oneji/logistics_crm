@extends('crm.layouts.main', [ 'title' => 'Заявки', 'requests_exp' => true ])

@section('stylesheets')
    <!-- dropify -->
    <link rel="stylesheet" href="{{ asset('crm/plugins/dropify/dist/css/dropify.css') }}">
    <style>
        .dropify-wrapper { height: 100px; }
        .request-alert { display: none; }
        .d-flex { display: flex; } 
        .align-items-center { align-items: center; } 
        .justify-content-start { justify-content: start; }
        .justify-content-center { justify-content: center; }
        .tax-color { background-color: rgb(227, 242, 253) !important; }
    </style>
@endsection

@section('content')
    <h3 class="heading_b uk-margin-bottom">Заявки</h3>
    <div class="md-fab-wrapper">
        <div class="md-fab md-fab-accent md-fab-sheet">
            <i class="material-icons">dns</i>
            <div class="md-fab-sheet-actions">
                @if (Auth::user()->can('create-request'))
                    <a href="#" class="md-color-white" data-uk-modal="{target:'#modal_header_footer'}"><i class="material-icons md-color-white">add</i> Добавить заявку</a>
                @endif
            </div>
        </div>
    </div>

    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <div class="uk-grid">
                <div class="uk-width-large-2-10 uk-width-small-1-1">
                    <select class="select-year" name="year" data-md-selectize id="monthSelect">
                        @foreach ($months as $key => $value)
                            <option value="{{ $key + 1 }}" {{ $key + 1 === (int) Date('n') ? 'selected' : '' }}>{{ $value }}</option>                                    
                        @endforeach
                    </select>
                </div>
                <div class="uk-width-large-2-10 uk-width-small-1-1">
                    <select class="select-year" name="year" data-md-selectize id="yearSelect">
                        @foreach ($years as $year)
                            <option value="{{ $year }}" {{ $year === (int) Date('Y') ? 'selected' : '' }}>{{ $year }}</option>                                    
                        @endforeach
                    </select>
                </div>
                <div class="uk-width-large-2-10 uk-width-small-1-1 d-flex align-items-center">
                    <a class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light pull-right" id="totalAmountBtn" href="#">Расчитать</a>                                                
                </div>
                <div class="uk-width-large-4-10 uk-width-small-1-1 d-flex align-items-center">
                    <div class="uk-alert totalAmount" data-uk-alert="" style="display: none;">
                        <h4 class="totalAmountLabel">Общая сумма прибыли за <span></span></h4>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="md-card-content">
        <div class="md-card">  
            <div class="loading">
                <img src="{{ asset('crm/img/spinner.gif') }}" alt="Spinner" />
            </div> 
            <div class="md-card-content">      
                <div class="uk-overflow-container">            
                    <table id="dt_default" class="uk-table requests-table" cellspacing="0" width="100%">  
                        <thead>
                            <tr>
                                <th>Заказчик</th>
                                <th>№ Заявки</th>
                                <th>Дата заявки</th>
                                <th>Сумма</th>
                                <th>Маршрут</th>
                                <th>Контакт</th>
                                @permission('download-request-doc')
                                    <th>Документ</th>
                                @endpermission
                                <th>Действия</th>
                            </tr>                                
                        </thead>
                        <tbody>
                            @foreach ($requests as $request)
                                <tr data-id="{{ $request['id'] }}">
                                    <td>{{ $request["client"] }}</td>
                                    <td>
                                        <span class="uk-badge uk-badge-success">{{ $request["no"] }}</span>
                                        @if ($request['type'] === 1)
                                            <span class="uk-badge uk-badge-primary">Налоговая</span>
                                        @endif
                                    </td>
                                    <td>{{ $months[date('n', strtotime($request['date'])) - 1] }} {{ date('d', strtotime($request['date'])) }}, {{ date('Y', strtotime($request['date'])) }}</td>
                                    <td>${{ $request["amount"] }}</td>
                                    <td>{{ $request["route"] }}</td>
                                    <td>{{ $request["contact"] }}</td>
                                    @permission('download-request-doc')
                                        <td>                                            
                                            @if (empty($request['document']))                                                
                                                <span class="uk-badge uk-badge-danger">Нет</span>
                                            @else
                                                <a href="{{ route('requests.download', $request['document']) }}" data-uk-tooltip title="Скачать документ"><i class="md-icon material-icons">file_download</i></a>
                                            @endif
                                        </td>
                                    @endpermission
                                    <td>
                                        {{-- @permission('update-request')
                                            <a href="#" class="edit-user" data-uk-tooltip title="Редактировать"><i class="md-icon material-icons">mode_edit</i></a>
                                        @endpermission --}}
                                        @permission('delete-request')
                                            <a href="#" class="delete-request" data-uk-tooltip title="Удалить" data-id="{{ $request['id'] }}"><i class="md-icon material-icons">delete</i></a>                                  
                                        @endpermission
                                        {{-- @if (!empty($request['document']))
                                            <a href="{{ route('requests.download', $request['document']) }}" data-uk-tooltip title="Скачать документ заявку"><i class="md-icon material-icons">file_download</i></a>
                                        @endif  --}}
                                    </td>
                                </tr>
                            @endforeach          
                        </tbody>
                    </table>
                </div>
            </div>   
        </div>
    </div>
@endsection

@section('modals')
    <div class="uk-modal" id="modal_header_footer">
        <div class="uk-modal-dialog">
            <div class="loading">
                <img src="{{ asset('crm/img/spinner.gif') }}" alt="Spinner" />
            </div>
            {!! Form::open([ 'url' => route('requests.store'), 'id' => 'request-form', 'class' => 'uk-form-stacked', 'enctype' => 'multipart/form-data' ]) !!}
                <div class="uk-modal-header">
                    <h3 class="uk-modal-title">Новая заявка</h3>
                </div>
                <div class="uk-grid" data-uk-grid-margin="">
                    <div class="uk-width-medium-1-3">
                        <div class="parsley-row">
                            <div class="md-input-wrapper">
                                {{ Form::label('client', 'Заказчик *') }}
                                {{ Form::text('client', '', [ 'required' => 'required', 'class' => 'md-input', 'data-parsley-id' => 4 ]) }}
                                <span class="md-input-bar"></span>
                            </div>                            
                        </div>
                    </div>
                    <div class="uk-width-medium-1-3">
                        <div class="parsley-row">
                            <div class="md-input-wrapper">
                                {{ Form::label('no', '№ заявки *') }}
                                {{ Form::number('no', '', [ 'required' => 'required', 'class' => 'md-input', 'data-parsley-id' => 6 ]) }}
                                <span class="md-input-bar"></span>
                            </div>                            
                        </div>
                    </div>
                    <div class="uk-width-medium-1-3 d-flex align-items-center justify-content-start">
                        <div class="parsley-row">
                            <input type="checkbox" data-switchery id="switch_demo_2" name="type" />
                            <label for="switch_demo_2" class="inline-label">Налоговая</label>  
                        </div>                     
                    </div>                    
                </div>
                <div class="uk-grid" data-uk-grid-margin="">
                    <div class="uk-width-medium-1-2">
                        <div class="parsley-row uk-margin-top">
                            <div class="uk-input-group">
                                <div class="md-input-wrapper">
                                    {{ Form::label('uk_dp_1', 'Дата заявки *') }}
                                    <span class="md-input-bar"></span>
                                    <input type="text" id="uk_dp_1" class="md-input" name="data" data-uk-datepicker="{format:'DD.MM.YYYY'}" />
                                </div>                                
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-2">
                        <div class="parsley-row uk-margin-top">
                            <div class="md-input-wrapper">
                                {{ Form::label('amount', 'Сумма *') }}
                                {{ Form::number('amount', '', [ 'required' => 'required', 'class' => 'md-input' ]) }}
                                <span class="md-input-bar"></span>
                            </div>                            
                        </div>
                    </div>                    
                </div>
                <div class="uk-grid" data-uk-grid-margin="">
                    <div class="uk-width-medium-1-2">
                        <div class="parsley-row uk-margin-top">
                            <div class="md-input-wrapper">
                                {{ Form::label('route', 'Маршрут *') }}
                                {{ Form::text('route', '', [ 'required' => 'required', 'class' => 'md-input', 'data-parsley-id' => 8 ]) }}
                                <span class="md-input-bar"></span>
                            </div>                            
                        </div>
                    </div>
                    <div class="uk-width-medium-1-2">
                        <div class="parsley-row uk-margin-top">
                            <div class="md-input-wrapper">
                                {{ Form::label('contact', 'Контакт *') }}
                                {{ Form::text('contact', '', [ 'required' => 'required', 'class' => 'md-input' ]) }}
                                <span class="md-input-bar"></span>
                            </div>                            
                        </div>
                    </div>
                    <div class="uk-width-medium-1-1">
                        <div class="parsley-row uk-margin-top">
                            <input type="file" id="input-file-a" class="dropify" name="document" />
                        </div>
                    </div>  
                    
                    <div class="uk-width-medium-1-1">
                        <div class="parsley-row uk-margin-top">
                            <div class="uk-alert request-alert" data-uk-alert="">
                                <a href="#" class="uk-alert-close uk-close"></a>
                                <span></span>
                            </div>
                        </div>
                    </div>                  
                      
                </div>            
                
                <div class="uk-modal-footer uk-text-right">
                    <button type="button" class="md-btn md-btn-flat uk-modal-close">Закрыть</button>
                    {{ Form::submit('Добавить заявку', [ 'class' => 'md-btn md-btn-flat md-btn-flat-primary' ]) }}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('scripts')
    <!-- datatables -->
    <script src="{{ asset('crm/plugins/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <!-- datatables custom integration -->
    <script src="{{ asset('crm/js/datatables_uikit.min.js') }}"></script>
    <!--  dropify -->
    <script src="{{ asset('crm/plugins/dropify/dist/js/dropify.min.js') }}"></script>
    <!-- inputmask -->
    <script src="{{ asset('crm/plugins/jquery.inputmask/dist/jquery.inputmask.bundle.js') }}"></script>
    <script>
        $(document).ready(function() {
            var _token = $('meta[name="csrf-token"]').attr('content');
            $maskedInput = $(".masked_input");
            $maskedInput.length && $maskedInput.inputmask()
            

            var $dt_default = $('#dt_default');
            if($dt_default.length) {
                $dt_default.DataTable({
                    language: {
                        lengthMenu: 'Показано _MENU_ записей',
                        search: 'Поиск: ',
                        zeroRecords: 'Ни одной записи не найдено',
                        paginate: {
                            first:      "Первая",
                            last:       "Последняя",
                            next:       "След.",
                            previous:   "Пред."
                        },
                        emptyTable: 'Данные в таблице не доступны',
                        info: 'Показано _START_ до _END_ из _TOTAL_ записей',
                        infoEmpty: 'Показано 0 до 0 из 0 записей'
                    }
                });
            }
            
            $(".dropify").dropify({
                messages: {
                    default: "Перетащите документ заявку сюда или кликните",
                    replace: "Перетащите документ заявку сюда или кликните чтобы заменить",
                    remove: "Удалить",
                    error: "Ошибка загрузки файла"
                }
            });
            
            $('#request-form').submit(function(e) {
                e.preventDefault();
                
                $('.loading').css('display', 'flex');

                var el = $(this);
                var formAction = el.attr('action');
                var formValues = el.serialize();

                $.ajax({
                    url: formAction,
                    type: 'POST',
                    data: new FormData(el[0]),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if(response.success) {
                            el[0].reset();
                            $('.loading').fadeOut(500);
                            $('.request-alert').removeClass('uk-alert-danger').addClass('uk-alert-success');
                            $('.request-alert span').text(response.message);
                            $('.request-alert').slideDown(500);                       
                            
                            setTimeout(function() {
                                $('.request-alert').slideUp(500);
                            }, 2000)
                        }
                    },
                    error: function(response) {                        
                        console.error(response);

                        if(response.status !== 500) {                            
                            $('.request-alert span').text(response.responseJSON.message);
                            
                        } else {
                            $('.request-alert span').text('Неизвестная ошибка на сервере. Повторите попытку.');
                        }   

                        $('.loading').fadeOut(500);
                        $('.request-alert').removeClass('.uk-alert-success').addClass('uk-alert-danger'); 
                        $('.request-alert').slideDown(500);              
                    } 
                });
            });

            $('#totalAmountBtn').click(function(e) {
                e.preventDefault();
                var month = $('#monthSelect').val();
                var year = $('#yearSelect').val();

                $.ajax({
                    url: '/admin/requests/total-amount?month=' + month + '&year=' + year,
                    type: 'GET',
                    success: function(response) {
                        if(response.success) {
                            // console.log(response);
                            $('.totalAmountLabel span').html(response.month + ', ' + year + ': <strong style="font-size: 130%;">$' + response.total + '</strong>');
                            $('.totalAmount').fadeIn(500);
                        }
                    },
                    error: function(response) {
                        console.error(response);
                    }
                });
            });

            $('.delete-request').click(function(e) {
                e.preventDefault();

                $('.loading').css('display', 'flex');
                var request_id = $(this).data('id');

                $.ajax({
                    url: 'requests/' + request_id,
                    type: 'DELETE',
                    data: { '_token': _token },
                    success: function(response) {
                        console.log(response);
                        
                        $('.requests-table tr[data-id="' + request_id + '"]').remove();
                        $('.loading').fadeOut(500);
                        UIkit.notify({
                            message : 'Успешно! ' + response.message,
                            status  : 'success',
                            timeout : 3000,
                            pos     : 'top-center'
                        });
                    },
                    error: function(response) {
                        console.log(response);

                        var message;
                        if(response.status === 500) {
                            message = 'Неизвестная ошибка на сервере.';
                        } else {
                            message = response.responseJSON.message;
                        }

                        $('.loading').fadeOut(500);
                        UIkit.notify({
                            message : 'Ошибка! ' + message,
                            status  : 'danger',
                            timeout : 3000,
                            pos     : 'top-center'
                        });
                    }
                });
            });
        });
    </script>
@endsection