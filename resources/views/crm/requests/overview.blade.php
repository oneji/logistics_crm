@extends('crm.layouts.main', [ 'title' => 'Заявки', 'requests_acc' => '' ])

@section('stylesheets')
    <style>
        .d-flex { display: flex; } 
        .align-items-center { align-items: center; } 
        .justify-content-start { justify-content: start; }
        .pa-0 { padding: 0 !important; }
        .ma-0 { margin: 0 !important; }
        .my-10 { margin: 10px 0 !important; }
        .selectize-control.single { margin-top: 0; }
        .selectize-control.single .selectize-input { padding: 8px 8px 0; }
    </style>
@endsection

@section('content')  
    <div id="top_bar">
        <div class="md-top-bar">
            <div class="uk-width-large-8-10 uk-container-center">
                <div class="uk-clearfix">
                    <div class="md-top-bar-actions-right d-flex align-items-center justify-content-start" style="width: 350px;">
                        @if (count($years) > 1)
                            <div class="uk-width-medium-1-2">
                                <p class="ma-0 uk-text-center">Заявки за:</p>
                            </div>
                            <div class="uk-width-medium-1-2">
                                {!! Form::open([ 'id' => 'filterForm', 'method' => 'GET', 'url' => '/admin/requests/overview' ]) !!}
                                    <div class="uk-input-group">                                     
                                        <div class="md-input-wrapper pa-0">
                                            <select class="select-year" name="y" data-md-selectize>
                                                @foreach ($years as $year)
                                                    <option value="{{ $year }}" {{ $year === (int) Date('Y') ? 'selected' : '' }}>{{ $year }}</option>                                    
                                                @endforeach
                                            </select>
                                        </div>
                                        <span class="uk-input-group-addon">
                                            <a href="#" id="submit-btn"><i class="material-icons"></i></a>
                                        </span>
                                    </div>
                                {!! Form::close() !!}
                            </div> 
                        @else
                            <div class="uk-width-medium-1-1">
                                <p class="my-10 uk-text-center">Заявки за текущий год</p>
                            </div>
                        @endif                                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="md-card-list-wrapper" id="mailbox">
        <div class="uk-width-large-8-10 uk-container-center">
            @if (count($temp) > 0)
                @foreach ($temp as $key => $value)
                    <div class="md-card-list">
                        <div class="md-card-list-header heading_list">{{ $months[$key - 1] }}</div>
                        <div class="md-card-list-header md-card-list-header-combined heading_list" style="display: none">All Messages</div>
                        <ul class="hierarchical_slide">
                            @foreach ($value as $k => $v)
                                <li class="{{ $v['type'] === 1 ? 'md-card-list-item-selected' : '' }}" data-id="{{ $v['id'] }}">
                                    <div class="md-card-list-item-menu">
                                        @if (!empty($v['document']) && Auth::user()->can('download-request-doc'))
                                            <a href="{{ route('requests.download', $v['document']) }}" class="md-icon material-icons" data-uk-tooltip title="Скачать документ заявку">file_download</a>                                        
                                        @endif
                                    </div>
                                    <span class="md-card-list-item-date">{{ $months[date('n', strtotime($v['date'])) - 1] }} {{ date('d', strtotime($v['date'])) }}, {{ date('Y', strtotime($v['date'])) }}</span>
                                    <div class="md-card-list-item-avatar-wrapper">
                                        <span class="md-card-list-item-avatar md-bg-{{ $v['type'] === 1 ? 'cyan' : 'amber-700' }}">{{ $v['no'] }}</span>
                                    </div>
                                    <div class="md-card-list-item-sender">
                                        <span><strong>Заказчик:</strong> {{ $v['client'] }}</span>
                                    </div>
                                    <div class="md-card-list-item-subject">
                                        <div class="md-card-list-item-sender-small">
                                            <span><strong>Заказчик:</strong> {{ $v['client'] }}</span>
                                        </div>
                                        <span><strong>Сумма:</strong> ${{ $v['amount'] }}</span>
                                    </div>
                                    <div class="md-card-list-item-content-wrapper">
                                        <div class="md-card-list-item-content">
                                            <p><strong>Номер заявки:</strong> #{{ $v['no'] }}</p>
                                            <p> <strong>Сумма:</strong> ${{ $v['amount'] }}</p>
                                            <p> <strong>Маршрут:</strong> {{ $v['route'] }}</p>
                                            <p> <strong>Контакт:</strong> {{ $v['contact'] }}</p>
                                            @if (!empty($v['document']) && Auth::user()->can('download-request-doc'))
                                                <p> <strong>Документ:</strong> <a href="{{ route('requests.download', $v['document']) }}">Скачать</a></p>                                            
                                            @endif
                                        </div>
                                    </div>
                                </li>                            
                            @endforeach
                        </ul>
                    </div>
                @endforeach         
            @else
                <div class="uk-alert" data-uk-alert="">
                    <a href="#" class="uk-alert-close uk-close"></a>
                    За выбранный год ни одной заявки не найдено!
                </div>
            @endif
        </div>
    </div>

@endsection

@section('scripts')
    <!--  mailbox functions -->
    <script src="{{ asset('crm/js/pages/requests/page_mailbox.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#submit-btn').click(function(e) {
                e.preventDefault();
                // $('#filterForm').attr('action', '/admin/requests/overview' + $('.select-year').val());
                $('#filterForm').submit();
            });
        });        
    </script>
@endsection