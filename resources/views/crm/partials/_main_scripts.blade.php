<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('crm/js/common.min.js') }}"></script>
<script src="{{ asset('crm/js/uikit_custom.min.js') }}"></script>
<script src="{{ asset('crm/js/altair_admin_common.min.js') }}"></script>

@yield('scripts')

<script>
    $(document).ready(function() {
        $('#password-form').submit(function(e) {
            e.preventDefault();
            $('.loading').css('display', 'flex');

            var el = $(this);
            var formAction = el.attr('action');
            var formValues = el.serialize();

            $.ajax({
                url: formAction,
                type: 'PUT',
                data: formValues,
                success: function(response) {
                    if(response.success) {
                        el[0].reset();
                        $('.loading').fadeOut(500);
                        $('.password-alert').removeClass('uk-alert-danger').addClass('uk-alert-success');
                        $('.password-alert span').text(response.message);
                        $('.password-alert').slideDown(500);                       
                        
                        setTimeout(function() {
                            $('.password-alert').slideUp(500);
                        }, 2000)
                    }
                    
                },
                error: function(response) {
                    if(response.status !== 500) {                            
                        $('.password-alert span').text(response.responseJSON.message);
                        
                    } else {
                        $('.password-alert span').text('Неизвестная ошибка на сервере. Повторите попытку.');
                    }   

                    $('.loading').fadeOut(500);
                    $('.password-alert').removeClass('.uk-alert-success').addClass('uk-alert-danger'); 
                    $('.password-alert').slideDown(500);             
                }
            });
        });
    });
</script>

<script>
    $(function() {
        if(isHighDensity()) {
            $.getScript( "{{ asset('crm/js/dense.min.js') }}", function(data) {
                // enable hires images
                altair_helpers.retina_images();
            });
        }
        if(Modernizr.touch) {
            // fastClick (touch devices)
            FastClick.attach(document.body);
        }
    });
    $window.load(function() {
        // ie fixes
        altair_helpers.ie_fix();
    });
</script>