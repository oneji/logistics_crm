<meta charset="UTF-8">
<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Remove Tap Highlight on Windows Phone IE -->
<meta name="msapplication-tap-highlight" content="no"/>
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{ $title }} | CUBE CRM </title>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,700"> 
<link rel="stylesheet" href="{{ asset('crm/css/uikit.almost-flat.min.css') }}" media="all">
<link rel="stylesheet" href="{{ asset('crm/css/style_switcher.min.css') }}" media="all">
<link rel="stylesheet" href="{{ asset('crm/css/main.min.css') }}" media="all">
<link rel="stylesheet" href="{{ asset('crm/css/themes_combined.min.css') }}" media="all">

<!-- matchMedia polyfill for testing media queries in JS -->
<!--[if lte IE 9]>
    <script type="text/javascript" src="{{ asset('plugins/matchMedia/matchMedia.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/matchMedia/matchMedia.addListener.js') }}"></script>
    <link rel="stylesheet" href="assets/css/ie.css" media="all">
<![endif]-->

<style>
    .password-alert { display: none; }
</style>

@yield('stylesheets')