@extends('crm.layouts.main', [ 'title' => 'Роли и доступы', 'acl_active' => 'act_item' ])

@section('content')
    <h3 class="heading_b uk-margin-bottom">Роли и права доступа</h3>
    
    <div class="md-fab-wrapper md-fab-speed-dial-horizontal">
        <a class="md-fab md-fab-primary" href="javascript:void(0)"><i class="material-icons">add</i></a>
        <div class="md-fab-wrapper-small">
            @permission('create-roles')
                <a class="md-fab md-fab-small md-fab-warning" href="javascript:void(0)" data-uk-modal="{target:'#roleModal'}" data-uk-tooltip title="Создать роль"><i class="material-icons">perm_identity</i></a>
                @endpermission
            @permission('create-acl')
                <a class="md-fab md-fab-small md-fab-success" href="javascript:void(0)" data-uk-modal="{target:'#permissionModal'}" data-uk-tooltip title="Создать право доступа"><i class="material-icons">security</i></a>
            @endpermission    
        </div>
    </div>

    @if (Session::has('permission.success'))
        <div class="uk-width-1-1">
            <div class="uk-alert uk-alert-success" data-uk-alert="">
                <a href="#" class="uk-alert-close uk-close"></a>
                {{ Session::get('permission.success') }}
            </div>
        </div>        
    @endif

    @if (Session::has('role.success'))
        <div class="uk-width-1-1">
            <div class="uk-alert uk-alert-success" data-uk-alert="">
                <a href="#" class="uk-alert-close uk-close"></a>
                {{ Session::get('role.success') }}
            </div>
        </div>        
    @endif

    <div class="uk-width-medium-2-3">
        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
            {!! Form::open([ 'url' => route('role.update'), 'id' => 'rolesUpdateForm' ]) !!}
                <div class="uk-grid uk-grid-divider" data-uk-grid-margin>
                    <div class="uk-width-large-1-1">
                        <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
                            <div class="uk-width-small-1-1 uk-width-large-1-3">
                                <ul class="uk-tab uk-tab-left" data-uk-tab="{connect:'#tabs_6', animation:'slide-right', swiping: false}">
                                    @foreach ($roles as $role)
                                        <li><a href="#">{{ $role['display_name'] }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="uk-width-small-1-1 uk-width-large-2-3">
                                <ul id="tabs_6" class="uk-switcher uk-margin-small-top">
                                    @foreach ($roles as $role)                                    
                                        <li>
                                            <ul class="md-list">
                                                @foreach ($permissions as $permission)   
                                                    <li>
                                                        <div class="md-list-content">
                                                            <div class="uk-float-right">
                                                                <input 
                                                                    type="checkbox" data-md-icheck 
                                                                    name="roles[{{ $role['id'] }}][permissions][{{ $permission['id'] }}]"
                                                                    id="{{ $role['name'] }}_{{ $permission['name'] }}"
                                                                    value="{{ $permission['id'] }}" 
                                                                    {{ $role->hasPermission($permission['name']) ? 'checked' : '' }} />
                                                            </div>
                                                            <span class="md-list-heading">{{ $permission['display_name'] }}</span>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </li> 
                                    @endforeach                                   
                                </ul>
                            </div>
                        </div>
                        <hr>
                        <div class="uk-width-1-1 uk-text-right">
                            <button type="submit" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light pull-right">Сохранить</button>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('modals')
    <!-- Modals -->
    @permission('create-acl')
    <div class="uk-modal" id="permissionModal">
        <div class="uk-modal-dialog">
            {!! Form::open([ 'url' => route('permission.store'), 'id' => 'permission-form', 'class' => 'uk-form-stacked' ]) !!}
                <div class="uk-modal-header">
                    <h3 class="uk-modal-title">Создать право доступа</h3>
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin="">
                        <div class="uk-width-medium-1-1">
                            <div class="parsley-row">
                                <div class="md-input-wrapper">
                                    {{ Form::label('name', 'Название права *') }}
                                    {{ Form::text('name', '', [ 'id' => 'permission_name', 'required' => 'required', 'class' => 'md-input', 'data-parsley-id' => 4 ]) }}
                                    <span class="md-input-bar"></span>
                                </div>                            
                            </div>
                        </div>                           
                    </div> 
                </div>

                <div class="uk-form-row">
                    <div class="uk-grid">
                        <div class="uk-width-medium-1-1">
                            <div class="parsley-row">
                                <div class="md-input-wrapper">
                                    {{ Form::label('display_name', 'Отображаемое имя') }}
                                    {{ Form::text('display_name', '', [ 'id' => 'permission_display_name', 'class' => 'md-input', 'data-parsley-id' => 6 ]) }}
                                    <span class="md-input-bar"></span>
                                </div>                            
                            </div>
                        </div>                            
                    </div>                    
                </div>

                <div class="uk-form-row">
                    <div class="uk-grid">
                        <div class="uk-width-medium-1-1">
                            <div class="parsley-row">
                                <div class="md-input-wrapper">
                                    {{ Form::label('description', 'Описание') }}
                                    {{ Form::text('description', '', [ 'id' => 'permission_description', 'class' => 'md-input', 'data-parsley-id' => 6 ]) }}
                                    <span class="md-input-bar"></span>
                                </div>                            
                            </div>
                        </div>   
                    </div>
                </div>

                <div class="uk-form-row">
                    <div class="uk-grid">
                        <div class="uk-width-medium-1-1">
                            <div class="parsley-row uk-margin-top">
                                <div class="uk-alert password-alert" data-uk-alert="">
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    <span></span>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                            
                
                <div class="uk-modal-footer uk-text-right">
                    <button type="button" class="md-btn md-btn-flat uk-modal-close">Закрыть</button>
                    {{ Form::submit('Создать', [ 'class' => 'md-btn md-btn-flat md-btn-flat-primary' ]) }}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
    @endpermission

    @permission('create-roles')
    <div class="uk-modal" id="roleModal">
        <div class="uk-modal-dialog">
            {!! Form::open([ 'url' => route('role.store'), 'id' => 'role-form', 'class' => 'uk-form-stacked' ]) !!}
                <div class="uk-modal-header">
                    <h3 class="uk-modal-title">Создать роль</h3>
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin="">
                        <div class="uk-width-medium-1-1">
                            <div class="parsley-row">
                                <div class="md-input-wrapper">
                                    {{ Form::label('name', 'Название роли *') }}
                                    {{ Form::text('name', '', [ 'id' => 'role_name', 'required' => 'required', 'class' => 'md-input', 'data-parsley-id' => 4 ]) }}
                                    <span class="md-input-bar"></span>
                                </div>                            
                            </div>
                        </div>                           
                    </div> 
                </div>

                <div class="uk-form-row">
                    <div class="uk-grid">
                        <div class="uk-width-medium-1-1">
                            <div class="parsley-row">
                                <div class="md-input-wrapper">
                                    {{ Form::label('display_name', 'Отображаемое имя') }}
                                    {{ Form::text('display_name', '', [ 'id' => 'role_display_name', 'class' => 'md-input', 'data-parsley-id' => 6 ]) }}
                                    <span class="md-input-bar"></span>
                                </div>                            
                            </div>
                        </div>                            
                    </div>                    
                </div>

                <div class="uk-form-row">
                    <div class="uk-grid">
                        <div class="uk-width-medium-1-1">
                            <div class="parsley-row">
                                <div class="md-input-wrapper">
                                    {{ Form::label('description', 'Описание') }}
                                    {{ Form::text('description', '', [ 'id' => 'role_description', 'class' => 'md-input', 'data-parsley-id' => 6 ]) }}
                                    <span class="md-input-bar"></span>
                                </div>                            
                            </div>
                        </div>   
                    </div>
                </div>

                <div class="uk-form-row">
                    <div class="uk-grid">
                        <div class="uk-width-medium-1-1">
                            <div class="parsley-row uk-margin-top">
                                <div class="uk-alert password-alert" data-uk-alert="">
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    <span></span>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                            
                
                <div class="uk-modal-footer uk-text-right">
                    <button type="button" class="md-btn md-btn-flat uk-modal-close">Закрыть</button>
                    {{ Form::submit('Создать', [ 'class' => 'md-btn md-btn-flat md-btn-flat-primary' ]) }}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
    @endpermission
@endsection

@section('scripts')
    <script>
        $('#rolesUpdateForm').submit(function(e) {
            e.preventDefault();
        
            var el = $(this);
            var formAction = el.attr('action');
            var formValues = el.serialize();
        
            $.ajax({
                url: formAction,
                type: 'PUT',
                data: formValues,
                success: function(response) {
                    console.log(response);
                    UIkit.notify({
                        message : 'Успешно! ' + response.message,
                        status  : 'success',
                        timeout : 3000,
                        pos     : 'top-center'
                    });
                },
                error: function(response) {
                    console.error(response);            
                }
            });
        });
    </script>
@endsection