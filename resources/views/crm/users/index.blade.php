@extends('crm.layouts.main', [ 'title' => 'Пользователи', 'users_active' => 'act_item' ])

@section('content')
    <h3 class="heading_b uk-margin-bottom">Пользователи</h3>
    <div class="md-fab-wrapper">
        <div class="md-fab md-fab-accent md-fab-sheet">
            <i class="material-icons">add</i>
            <div class="md-fab-sheet-actions">
                <a href="#" class="md-color-white" data-uk-modal="{target:'#addUserModal'}"><i class="material-icons md-color-white">add</i> Добавить пользователя</a>
            </div>
        </div>
    </div>

    @if (Session::has('user.created'))
        <div class="uk-width-1-1">
            <div class="uk-alert uk-alert-success" data-uk-alert="">
                <a href="#" class="uk-alert-close uk-close"></a>
                {{ Session::get('user.created') }}
            </div>
        </div>        
    @endif

    <div class="uk-width-medium-2-3">
        <div class="md-card-content">
            <div class="md-card">   
                <div class="loading">
                    <img src="{{ asset('crm/img/spinner.gif') }}" alt="Spinner" />
                </div>
                <div class="md-card-content">      
                    <div class="uk-overflow-container">            
                        <table id="dt_default" class="uk-table user-table" cellspacing="0" width="100%">  
                            <thead>
                                <tr>
                                    <th>Имя пользователя</th>
                                    <th>Роль</th>
                                    <th>Статус</th>
                                    <th>Действия</th>
                                </tr>                                
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    <tr data-id="{{ $user['id'] }}">
                                        <td>{{ $user['username'] }}</td>
                                        <td>
                                            @foreach ($user->roles as $role)                                                
                                                @if ($role['name'] === 'superadministrator' || $role['name'] === 'administrator')
                                                    <span class="uk-badge uk-badge-success">{{ $role['display_name'] }}</span>
                                                @else
                                                    <span class="uk-badge uk-badge-primary">{{ $role['display_name'] }}</span>
                                                @endif                                     
                                            @endforeach
                                        </td>
                                        <td class="user-status">
                                            @if ($user['active'] === 1)
                                                <span class="uk-badge uk-badge-success">Активен</span>
                                            @else
                                                <span class="uk-badge uk-badge-danger">Неактивен</span>
                                            @endif   
                                        </td>
                                        <td>
                                            {{-- @permission('update-users')
                                                <a href="#" class="edit-user" data-uk-tooltip title="Редактировать"><i class="md-icon material-icons">mode_edit</i></a>
                                            @endpermission --}}
                                            @if ($user['active'] === 1)
                                                <a href="#" class="change-status" data-status="0" data-uk-tooltip title="Деактивировать"><i class="md-icon material-icons">lock</i></a>
                                            @else
                                                <a href="#" class="change-status" data-status="1" data-uk-tooltip title="Активировать"><i class="md-icon material-icons">lock_open</i></a>
                                            @endif  
                                            @permission('delete-users')
                                                <a href="#" class="remove-user" data-uk-tooltip title="Удалить"><i class="md-icon material-icons">delete</i></a> 
                                            @endpermission                                 
                                        </td>
                                    </tr>
                                @endforeach          
                            </tbody>
                        </table>
                    </div>
                </div>   
            </div>
        </div>
    </div>
@endsection

@section('modals')
    <div class="uk-modal" id="addUserModal">
        <div class="uk-modal-dialog">
            <div class="loading">
                <img src="{{ asset('crm/img/spinner.gif') }}" alt="Spinner" />
            </div>
            {!! Form::open([ 'url' => route('users.store'), 'id' => 'users-form', 'class' => 'uk-form-stacked' ]) !!}
                <div class="uk-modal-header">
                    <h3 class="uk-modal-title">Создать пользователя</h3>
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin="">
                        <div class="uk-width-medium-1-1">
                            <div class="parsley-row">
                                <div class="md-input-wrapper">
                                    {{ Form::label('username', 'Имя пользователя *') }}
                                    {{ Form::text('username', '', [ 'required' => 'required', 'class' => 'md-input', 'data-parsley-id' => 4 ]) }}
                                    <span class="md-input-bar"></span>
                                </div>                            
                            </div>
                        </div>                           
                    </div> 
                </div>

                <div class="uk-form-row">
                    <div class="uk-grid">
                        <div class="uk-width-medium-1-1">
                            <div class="parsley-row">
                                <div class="md-input-wrapper">
                                    {{ Form::label('password', 'Пароль *') }}
                                    {{ Form::password('password', [ 'required' => 'required', 'class' => 'md-input', 'data-parsley-id' => 6 ]) }}
                                    <span class="md-input-bar"></span>
                                </div>                            
                            </div>
                        </div>                            
                    </div>                    
                </div>

                <div class="uk-form-row">
                    <div class="uk-grid">
                        <div class="uk-width-medium-1-1">
                            <div class="parsley-row">
                                <div class="md-input-wrapper">
                                    {{ Form::label('password_confirmation', 'Повторите пароль *') }}
                                    {{ Form::password('password_confirmation', [ 'required' => 'required', 'class' => 'md-input', 'data-parsley-id' => 6 ]) }}
                                    <span class="md-input-bar"></span>
                                </div>                            
                            </div>
                        </div>   
                    </div>
                </div>

                <div class="uk-form-row">
                    <div class="uk-grid">
                        <div class="uk-width-medium-1-1">
                            <div class="parsley-row">
                                <div class="md-input-wrapper">
                                    <select name="role" class="md-input" required>
                                        <option value="" disabled selected hidden>Выберите роль</option>
                                        @foreach ($roles as $role)
                                            <option value="{{ $role['id'] }}">{{ $role['display_name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>                            
                            </div>
                        </div>   
                    </div>
                </div>

                <div class="uk-form-row">
                    <div class="uk-grid">
                        <div class="uk-width-medium-1-1">
                            <div class="parsley-row uk-margin-top">
                                <div class="uk-alert password-alert" data-uk-alert="">
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    <span></span>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>                        
                
                <div class="uk-modal-footer uk-text-right">
                    <button type="button" class="md-btn md-btn-flat uk-modal-close">Закрыть</button>
                    {{ Form::submit('Создать', [ 'class' => 'md-btn md-btn-flat md-btn-flat-primary' ]) }}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('scripts')
    <!-- datatables -->
    <script src="{{ asset('crm/plugins/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <!-- datatables custom integration -->
    <script src="{{ asset('crm/js/datatables_uikit.min.js') }}"></script>
    <script>
        var _token = $('meta[name="csrf-token"]').attr('content');
        var $dt_default = $('#dt_default');
        if($dt_default.length) {
            $dt_default.DataTable({
                language: {
                    lengthMenu: 'Показано _MENU_ записей',
                    search: 'Поиск: ',
                    zeroRecords: 'Ни одной записи не найдено',
                    paginate: {
                        first:      "Первая",
                        last:       "Последняя",
                        next:       "След.",
                        previous:   "Пред."
                    },
                    emptyTable: 'Данные в таблице не доступны',
                    info: 'Показано _START_ до _END_ из _TOTAL_ записей',
                    infoEmpty: 'Показано 0 до 0 из 0 записей'
                }
            });
        }

        $(document).on('click', '.change-status', function(e) {
            e.preventDefault();
            $('.loading').css('display', 'flex');
            
            var user_id = $(this).parent().parent().data('id');
            var status = $(this).data('status');           

            $.ajax({
                url: 'users/status',
                type: 'POST',
                data: { 'id': user_id, 'status': status, '_token': _token },
                success: function(response) {
                    $('.loading').fadeOut(500);
                    var oldClass, newClass, icon, newStatus, tooltipTitle, statusText;

                    if(status === 0) {
                        oldClass        = 'uk-badge-success';
                        newClass        = 'uk-badge-danger';
                        icon            = 'lock_open';
                        tooltipTitle    = 'Активировать';
                        statusText      = 'Неактивен';
                        newStatus       = 1;                        

                    } else if(status === 1) {
                        oldClass        = 'uk-badge-danger';
                        newClass        = 'uk-badge-success';
                        icon            = 'lock';
                        tooltipTitle    = 'Деактивировать';
                        statusText      = 'Активен';
                        newStatus       = 0;
                    }

                    $('.user-table tr[data-id="' + user_id + '"]').find('td.user-status span').text(statusText).removeClass(oldClass).addClass(newClass);
                    $('.user-table tr[data-id="' + user_id + '"]').find('a.change-status').data('status', newStatus).attr('title', tooltipTitle);
                    $('.user-table tr[data-id="' + user_id + '"]').find('a.change-status > i').text(icon);

                    UIkit.notify({
                        message : 'Успешно! ' + response.message,
                        status  : 'success',
                        timeout : 3000,
                        pos     : 'top-center'
                    });
                },
                error: function(response) {
                    console.error(response);
                    
                    $('.loading').fadeOut(500);
                    UIkit.notify({
                        message : 'Ошибка! ' + response.responseJSON.message,
                        status  : 'danger',
                        timeout : 3000,
                        pos     : 'top-center'
                    });
                }
            });           
        });

        $(document).on('click', '.remove-user', function(e) {
            e.preventDefault();
            $('.loading').css('display', 'flex');

            var user_id = $(this).parent().parent().data('id');

            $.ajax({
                url: 'users/' + user_id,
                type: 'DELETE',
                data: { '_token': _token },
                success: function(response) {
                    console.log(response);

                    $('.user-table tr[data-id="' + user_id + '"]').remove();
                    $('.loading').fadeOut(500);
                    UIkit.notify({
                        message : 'Успешно! ' + response.message,
                        status  : 'success',
                        timeout : 3000,
                        pos     : 'top-center'
                    });
                },
                error: function(response) {
                    console.error(response);                    
                }
            });
        });
    </script>
@endsection