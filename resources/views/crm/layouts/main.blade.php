<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="ru"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="ru"> <!--<![endif]-->
<head>
    @include('crm.partials._head', [ 'title' => isset($title) ? $title : 'Главная' ])
</head>
<body class="disable_transitions sidebar_main_open sidebar_main_swipe" style="background-color: #fafafa">
    <!-- main header -->
    <header id="header_main">
        <div class="header_main_content">
            <nav class="uk-navbar">
                                
                <!-- main sidebar switch -->
                <a href="#" id="sidebar_main_toggle" class="sSwitch sSwitch_left">
                    <span class="sSwitchIcon"></span>
                </a>
                
                <!-- secondary sidebar switch -->
                <a href="#" id="sidebar_secondary_toggle" class="sSwitch sSwitch_right sidebar_secondary_check">
                    <span class="sSwitchIcon"></span>
                </a>
                
                {{-- <div id="menu_top_dropdown" class="uk-float-left uk-hidden-small">
                    <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}">
                        <a href="#" class="top_menu_toggle"><i class="material-icons md-24">&#xE8F0;</i></a>
                        <div class="uk-dropdown uk-dropdown-width-3">
                            <div class="uk-grid uk-dropdown-grid">
                                <div class="uk-width-2-3">
                                    <div class="uk-grid uk-grid-width-medium-1-3 uk-margin-bottom uk-text-center">
                                        <a href="page_mailbox.html" class="uk-margin-top">
                                            <i class="material-icons md-36 md-color-light-green-600">&#xE158;</i>
                                            <span class="uk-text-muted uk-display-block">Mailbox</span>
                                        </a>
                                        <a href="page_invoices.html" class="uk-margin-top">
                                            <i class="material-icons md-36 md-color-purple-600">&#xE53E;</i>
                                            <span class="uk-text-muted uk-display-block">Invoices</span>
                                        </a>
                                        <a href="page_chat.html" class="uk-margin-top">
                                            <i class="material-icons md-36 md-color-cyan-600">&#xE0B9;</i>
                                            <span class="uk-text-muted uk-display-block">Chat</span>
                                        </a>
                                        <a href="page_scrum_board.html" class="uk-margin-top">
                                            <i class="material-icons md-36 md-color-red-600">&#xE85C;</i>
                                            <span class="uk-text-muted uk-display-block">Scrum Board</span>
                                        </a>
                                        <a href="page_snippets.html" class="uk-margin-top">
                                            <i class="material-icons md-36 md-color-blue-600">&#xE86F;</i>
                                            <span class="uk-text-muted uk-display-block">Snippets</span>
                                        </a>
                                        <a href="page_user_profile.html" class="uk-margin-top">
                                            <i class="material-icons md-36 md-color-orange-600">&#xE87C;</i>
                                            <span class="uk-text-muted uk-display-block">User profile</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="uk-width-1-3">
                                    <ul class="uk-nav uk-nav-dropdown uk-panel">
                                        <li class="uk-nav-header">Components</li>
                                        <li><a href="components_accordion.html">Accordions</a></li>
                                        <li><a href="components_buttons.html">Buttons</a></li>
                                        <li><a href="components_notifications.html">Notifications</a></li>
                                        <li><a href="components_sortable.html">Sortable</a></li>
                                        <li><a href="components_tabs.html">Tabs</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
                
                <div class="uk-navbar-flip">
                    <ul class="uk-navbar-nav user_actions">
                        <li><a href="#" id="full_screen_toggle" class="user_action_icon uk-visible-large"><i class="material-icons md-24 md-light">&#xE5D0;</i></a></li>
                        {{-- <li><a href="#" id="main_search_btn" class="user_action_icon"><i class="material-icons md-24 md-light">&#xE8B6;</i></a></li> --}}
                        {{-- <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                            <a href="#" class="user_action_icon"><i class="material-icons md-24 md-light">&#xE7F4;</i><span class="uk-badge">16</span></a>
                            <div class="uk-dropdown uk-dropdown-xlarge">
                                <div class="md-card-content">
                                    <ul class="uk-tab uk-tab-grid" data-uk-tab="{connect:'#header_alerts',animation:'slide-horizontal'}">
                                        <li class="uk-width-1-2 uk-active"><a href="#" class="js-uk-prevent uk-text-small">Messages (12)</a></li>
                                        <li class="uk-width-1-2"><a href="#" class="js-uk-prevent uk-text-small">Alerts (4)</a></li>
                                    </ul>
                                    <ul id="header_alerts" class="uk-switcher uk-margin">
                                        <li>
                                            <ul class="md-list md-list-addon">
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <span class="md-user-letters md-bg-cyan">kp</span>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><a href="page_mailbox.html">Voluptas id.</a></span>
                                                        <span class="uk-text-small uk-text-muted">Libero voluptas voluptatibus ratione culpa a in dolor qui hic ipsam et.</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <img class="md-user-image md-list-addon-avatar" src="{{ asset('crm/img/avatars/user.png') }}" alt=""/>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><a href="page_mailbox.html">Esse iusto.</a></span>
                                                        <span class="uk-text-small uk-text-muted">Nesciunt doloribus rerum cum ut aperiam.</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <span class="md-user-letters md-bg-light-green">zc</span>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><a href="page_mailbox.html">Odit ipsam.</a></span>
                                                        <span class="uk-text-small uk-text-muted">Quasi et non officiis pariatur aut aspernatur sit.</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <img class="md-user-image md-list-addon-avatar" src="{{ asset('crm/img/avatars/user.png') }}" alt=""/>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><a href="page_mailbox.html">Exercitationem fugiat.</a></span>
                                                        <span class="uk-text-small uk-text-muted">Adipisci occaecati blanditiis sapiente autem quo eligendi consequuntur consectetur.</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <img class="md-user-image md-list-addon-avatar" src="{{ asset('crm/img/avatars/user.png') }}" alt=""/>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><a href="page_mailbox.html">Et unde.</a></span>
                                                        <span class="uk-text-small uk-text-muted">Minus atque eum laboriosam libero facere tenetur voluptates at possimus.</span>
                                                    </div>
                                                </li>
                                            </ul>
                                            <div class="uk-text-center uk-margin-top uk-margin-small-bottom">
                                                <a href="page_mailbox.html" class="md-btn md-btn-flat md-btn-flat-primary js-uk-prevent">Show All</a>
                                            </div>
                                        </li>
                                        <li>
                                            <ul class="md-list md-list-addon">
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons uk-text-warning">&#xE8B2;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">Modi et eos.</span>
                                                        <span class="uk-text-small uk-text-muted uk-text-truncate">Enim exercitationem temporibus vero voluptates enim adipisci velit sint.</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons uk-text-success">&#xE88F;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">Cum nam sed.</span>
                                                        <span class="uk-text-small uk-text-muted uk-text-truncate">Dolore nulla vel ea voluptas qui nulla adipisci.</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons uk-text-danger">&#xE001;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">Quia voluptatem.</span>
                                                        <span class="uk-text-small uk-text-muted uk-text-truncate">Doloribus exercitationem consequatur aspernatur qui corrupti fugit sint.</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons uk-text-primary">&#xE8FD;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">Aliquam voluptate adipisci.</span>
                                                        <span class="uk-text-small uk-text-muted uk-text-truncate">Exercitationem sed harum soluta provident neque deleniti inventore omnis.</span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li> --}}
                        <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                            <a href="#" class="user_action_image"><img class="md-user-image" src="{{ asset('crm/img/avatars/no-avatar.png') }}" alt="Фото" /></a>
                            <div class="uk-dropdown uk-dropdown-small">
                                <ul class="uk-nav js-uk-prevent">
                                    <li><a href="#" data-uk-modal="{target:'#changePassModal'}">Изменить пароль</a></li>
                                    <li>
                                        {!! Form::open([ 'url' => route('logout'), 'method' => 'POST' ]) !!}
                                            {{ Form::submit('Выйти', [ 'class' => 'logout_btn' ]) }}
                                        {!! Form::close() !!}
                                    </li>                                    
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <div class="header_main_search_form">
            <i class="md-icon header_main_search_close material-icons">&#xE5CD;</i>
            <form class="uk-form uk-autocomplete" data-uk-autocomplete="{source:'data/search_data.json'}">
                <input type="text" class="header_main_search_input" />
                <button class="header_main_search_btn uk-button-link"><i class="md-icon material-icons">&#xE8B6;</i></button>
                <script type="text/autocomplete">
                    {{--  <ul class="uk-nav uk-nav-autocomplete uk-autocomplete-results">
                        {{~items}}
                        <li data-value="{{ $item.value }}">
                            <a href="{{ $item.url }}" class="needsclick">
                                {{ $item.value }}<br>
                                <span class="uk-text-muted uk-text-small">{{{ $item.text }}}</span>
                            </a>
                        </li>
                        {{/items}}
                        <li data-value="autocomplete-value">
                            <a class="needsclick">
                                Autocomplete Text<br>
                                <span class="uk-text-muted uk-text-small">Helper text</span>
                            </a>
                        </li>
                    </ul>  --}}
                </script>
            </form>
        </div>
    </header><!-- main header end -->
    <!-- main sidebar -->
    <aside id="sidebar_main">
        
        <div class="sidebar_main_header">
            <div class="sidebar_logo">
                <a href="{{ route('crm.dashboard') }}" class="sSidebar_hide sidebar_logo_large">
                    <img class="logo_regular" src="{{ asset('crm/img/logo_main.png') }}" alt="CUBE logo" style="width: 80%;" />
                </a>
                <a href="{{ route('crm.dashboard') }}" class="sSidebar_show sidebar_logo_small">
                    <img class="logo_regular" src="{{ asset('crm/img/logo_main_small.png') }}" alt="" height="32" width="32"/>
                    <img class="logo_light" src="{{ asset('crm/img/logo_main_small_light.png') }}" alt="" height="32" width="32"/>
                </a>
            </div>
        </div>
        
        <div class="menu_section">
            <ul>
                <li title="Перейти на сайт">
                    <a href="{{ route('site.home') }}">
                        <span class="menu_icon"><i class="material-icons">language</i></span>
                        <span class="menu_title">Перейти на сайт</span>
                    </a>                    
                </li>
                <li class="{{ isset($home_active) ? $home_active : '' }}" title="Главная">
                    <a href="{{ route('crm.dashboard') }}">
                        <span class="menu_icon"><i class="material-icons">home</i></span>
                        <span class="menu_title">Главная</span>
                    </a>                    
                </li>

                @if(Auth::user()->can('read-requests') && !Auth::user()->hasRole('superadministrator|administrator'))
                    <li class="{{ isset($requests_exp) ? 'current_section' : '' }}" title="Заявки">
                        <a href="{{ route('requests.index') }}">
                            <span class="menu_icon"><i class="material-icons">assignment</i></span>
                            <span class="menu_title">Заявки</span>
                        </a>                    
                    </li> 
                @endif

                @permission('create-roles')
                    <li title="Пользователи" class="submenu_trigger" style="">
                        <a href="#">
                            <span class="menu_icon"><i class="material-icons">group</i></span>
                            <span class="menu_title">Пользователи</span>
                        </a>
                        <ul style="display: none;" class="">
                            <li class="{{ isset($users_active) ? $users_active : '' }}"><a href="{{ route('users.index') }}">Список пользователей</a></li>
                            <li class="{{ isset($acl_active) ? $acl_active : '' }}"><a href="{{ route('acl.index') }}">Роли и права</a></li>
                        </ul>                    
                    </li>
                @endpermission

                @role('superadministrator|administrator')                
                    <li title="Заявки" class="submenu_trigger" style="">
                        <a href="#">
                            <span class="menu_icon"><i class="material-icons">assignment</i></span>
                            <span class="menu_title">Заявки</span>
                        </a>
                        <ul style="display: none;" class="">
                            <li class="{{ isset($requests_exp) ? 'act_item' : '' }}"><a href="{{ route('requests.index') }}">Заявки экспедитора</a></li>
                            <li class="{{ isset($requests_acc) ? 'act_item' : '' }}"><a href="{{ route('requests.overview') }}">Заявки бухгалтера</a></li>
                        </ul>                    
                    </li> 
                @endrole
                
                {{-- @permission('read-employees')
                    <li class="{{ isset($employee_active) ? $employee_active : '' }}" title="Сотрудники">
                        <a href="{{ route('employees.index') }}">
                            <span class="menu_icon"><i class="material-icons">assignment_ind</i></span>
                            <span class="menu_title">Сотрудники</span>
                        </a>                    
                    </li>
                @endpermission --}}

                @role('accountant')                      
                    <li class="{{ isset($requests_acc) ? 'current_section' : '' }}"  title="Заявки">
                        <a href="{{ route('requests.overview') }}">
                            <span class="menu_icon"><i class="material-icons">assignment</i></span>
                            <span class="menu_title">Заявки</span>
                        </a>                    
                    </li>                  
                @endrole
                
            </ul>
        </div>
    </aside><!-- main sidebar end -->

    <div id="app">
        <div id="page_content">
            <div id="page_content_inner">
                @yield('content')
            </div>
        </div>
    </div>  

    <!-- Modals -->
    <div class="uk-modal" id="changePassModal">
        <div class="uk-modal-dialog">
            <div class="loading">
                <img src="{{ asset('crm/img/spinner.gif') }}" alt="Spinner" />
            </div>
            {!! Form::open([ 'url' => route('password.change'), 'id' => 'password-form', 'class' => 'uk-form-stacked' ]) !!}
                <div class="uk-modal-header">
                    <h3 class="uk-modal-title">Изменить пароль</h3>
                </div>
                <div class="uk-form-row">
                    <div class="uk-grid" data-uk-grid-margin="">
                        <div class="uk-width-medium-1-1">
                            <div class="parsley-row">
                                <div class="md-input-wrapper">
                                    {{ Form::label('old_password', 'Cтарый пароль *') }}
                                    {{ Form::password('old_password', [ 'required' => 'required', 'class' => 'md-input', 'data-parsley-id' => 4 ]) }}
                                    <span class="md-input-bar"></span>
                                </div>                            
                            </div>
                        </div>                           
                    </div> 
                </div>

                <div class="uk-form-row">
                    <div class="uk-grid">
                        <div class="uk-width-medium-1-1">
                            <div class="parsley-row">
                                <div class="md-input-wrapper">
                                    {{ Form::label('new_password', 'Новый пароль *') }}
                                    {{ Form::password('new_password', [ 'required' => 'required', 'class' => 'md-input', 'data-parsley-id' => 6 ]) }}
                                    <span class="md-input-bar"></span>
                                </div>                            
                            </div>
                        </div>                            
                    </div>                    
                </div>

                <div class="uk-form-row">
                    <div class="uk-grid">
                        <div class="uk-width-medium-1-1">
                            <div class="parsley-row">
                                <div class="md-input-wrapper">
                                    {{ Form::label('new_password_confirmation', 'Повторите новый пароль *') }}
                                    {{ Form::password('new_password_confirmation', [ 'required' => 'required', 'class' => 'md-input', 'data-parsley-id' => 6 ]) }}
                                    <span class="md-input-bar"></span>
                                </div>                            
                            </div>
                        </div>   
                    </div>
                </div>

                <div class="uk-form-row">
                    <div class="uk-grid">
                        <div class="uk-width-medium-1-1">
                            <div class="parsley-row uk-margin-top">
                                <div class="uk-alert password-alert" data-uk-alert="">
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    <span></span>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                         
                
                <div class="uk-modal-footer uk-text-right">
                    <button type="button" class="md-btn md-btn-flat uk-modal-close">Закрыть</button>
                    {{ Form::submit('Изменить', [ 'class' => 'md-btn md-btn-flat md-btn-flat-primary' ]) }}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
    
    @yield('modals')

    @include('crm.partials._main_scripts')
</body>
</html>