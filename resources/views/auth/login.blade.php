<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Вход | CUBE CRM</title>
    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('crm/css/uikit.almost-flat.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('crm/css/login_page.min.css') }}" />
</head>
<body class="login_page login_page_v2">

        <div class="login_page_wrapper" id="app">
                <div class="md-card" id="login_card">
                    <div class="md-card-content large-padding" id="login_form">
                        <div class="login_heading">
                            <div class="user_avatar"></div>
                        </div>
                        @if (Session::has('user.notfound'))
                            <div class="uk-alert uk-alert-danger" data-uk-alert="">
                                <a href="#" class="uk-alert-close uk-close"></a>
                                {{ Session::get('user.notfound') }}
                            </div>    
                        @endif
                        
                        @if ($errors->has('password') > 0)
                            <div class="uk-alert uk-alert-danger" data-uk-alert="">
                                <a href="#" class="uk-alert-close uk-close"></a>
                                Неверный пароль.
                            </div>    
                        @endif
                        
                        {!! Form::open([ 'url' => '/login', 'method' => 'POST' ]) !!}
                            <div class="uk-form-row">
                                {{ Form::label('username', 'Имя пользователя') }}
                                {{ Form::text('username', old('username'), [ 'class' => 'md-input', 'required' => 'required' ]) }}
                            </div>

                            <div class="uk-form-row">
                                {{ Form::label('password', 'Пароль') }}
                                {{ Form::password('password', [ 'class' => 'md-input', 'required' => 'required', 'data-parsley-minlength' => '6', 'data-parsley-trigger' => 'keyup', 'data-parsley-validation-threshold' => '10', 'data-parsley-minlength-message' => 'Пароль должен состоять из не менее 6 символов.' ]) }}
                            </div>

                            <div class="uk-margin-medium-top">
                                {{ Form::submit('Войти', [ 'class' => 'md-btn md-btn-primary md-btn-block md-btn-large' ]) }}
                            </div>                                        
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

    @include('crm.partials._main_scripts')

    <script src="{{ asset('crm/js/login.min.js') }}"></script>    
    {{--  <script>
        altair_forms.parsley_validation_config();
    </script>  --}}
    {{--  <script src="{{ asset('plugins/parsleyjs/dist/parsley.min.js') }}"></script>
    <script src="{{ asset('js/forms_validation.min.js') }}"></script>  --}}        
</body>
</html>    