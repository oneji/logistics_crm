$(document).ready(function() {
    $('#employee_edit_form').submit(function(e) {
        e.preventDefault();
        var el = $(this);
        var formAction = el.attr('action');
        var formValues = el.serialize();
        var formMethod = el.find('input[name="_method"]').val();

        $.ajax({
            url: formAction,
            type: formMethod,
            data: new FormData(el[0]),
            processData: false,
            contentType: false,
            success: function() {

            },
            error: function() {
                
            }
        })


    });
});