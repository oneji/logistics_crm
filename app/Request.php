<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client', 'no', 'amount', 'route', 'contact', 'type',
    ];

    /**
     * Disabling default timestamps in the database table.
     *
     * @var boolean
     */
    public $timestamps = false;
}
