<?php

namespace App\Http\Controllers\Crm;

use App\User;
use App\Role;
use App\Permission;
use App\Request as Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requests = Requests::all();
        $users = User::all();
        $roles = Role::all();
        $permissions = Permission::all();

        return view('crm.pages.home')->with([
            'requests' => $requests,
            'users' => $users,
            'roles' => $roles,
            'permissions' => $permissions
        ]);
    }
}
