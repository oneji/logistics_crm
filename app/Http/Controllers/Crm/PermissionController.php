<?php

namespace App\Http\Controllers\Crm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Permission;
use Validator;
use Session;

class PermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:superadministrator');
    }

    /**
     * Update the permission in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|unique:permissions',
            'display_name' => 'nullable|string',
            'description' => 'nullable|string'
        ]);

        if($validator->fails()) {
            $errors = $validator->messages()->toJson();
            
            Session::flash('permission.fail', '');
            return redirect()->route('acl.index');
        }

        $permission = new Permission($request->all());
        $permission->save();

        Session::flash('permission.success', 'Право доступа успешно создано.');
        return redirect()->route('acl.index');
    }
}
