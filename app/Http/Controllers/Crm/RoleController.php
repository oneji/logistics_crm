<?php

namespace App\Http\Controllers\Crm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use Validator;
use Session;

class RoleController extends Controller
{
    /**
     * Store the role in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|unique:roles',
            'display_name' => 'nullable|string',
            'description' => 'nullable|string'
        ]);

        if($validator->fails()) {
            $errors = $validator->messages()->toJson();
            
            Session::flash('role.fail', '');
            return redirect()->route('acl.index');
        }

        $role = new Role($request->all());
        $role->save();

        Session::flash('role.success', 'Роль успешно создана.');
        return redirect()->route('acl.index');
    }

    /**
     * Update roles in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
    public function update(Request $request)
    {
        if($request->roles === null) {
            return response()->json([
                'success' => true,
                'message' => 'Ни одного права не было выбрано!'
            ]);
        } 
        
        foreach($request->roles as $roleID => $permission) {
            $role = Role::find($roleID);  
            $role->syncPermissions($permission['permissions']);
        }

        return response()->json([
            'success' => true,
            'message' => 'Права доступа успешно установлены.',
            'data' => $request->all()
        ]);
    }
}
