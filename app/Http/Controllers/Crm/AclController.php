<?php

namespace App\Http\Controllers\Crm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Permission;
use App\Role;

class AclController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:create-roles');
    }

    /**
     * Display a listing of permissions and roles.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = Permission::all();
        $roles = Role::all();
        return view('crm.acl.index')->with([ 'permissions' => $permissions, 'roles' => $roles ]);
    }
}
