<?php

namespace App\Http\Controllers\Crm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use App\Request as Requests;
use Validator;
use Carbon\Carbon;

class RequestController extends Controller
{
    public $months = [ 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь' ];                
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $months = [ 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь' ];                
        $requests = Requests::orderBy('id', 'desc')->get();
        $years = $this->getYears();

        return view('crm.requests.index')->with([ 'requests' => $requests, 'years' => $years, 'months' => $this->months ]);
    }

    public function apiGetRequests()
    {
        $requests = Requests::all();
        return response()->json($requests);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'client' => 'required',
            'no' => 'integer|unique:requests',
            'data' => 'required',
            'amount' => 'required',
            'route' => 'required|max:191',
            'contact' => 'required',
            'document' => 'file|nullable'
        ]);  
        
        if($validator->fails()) {
            $errors = $validator->messages()->toJson();
            return response()->json([
                'success' => false,
                'message' => $errors
            ], 403);
        }

        if($request->hasFile('document')){            
            $fileFullName = $request->file('document')->getClientOriginalName(); 
            $filename = pathinfo($fileFullName, PATHINFO_FILENAME);
            $filePath = $request->file('document')->path();
            $fileExtension = $request->file('document')->getClientOriginalExtension();
            $fileNameToStore = 'doc_'.$request->no.'.'.$fileExtension;
            $path = $request->file('document')->move(public_path('/crm/uploads/documents'), $fileNameToStore);  
        } else {
            $fileNameToStore = null;
        } 

        $type = 0;
        if($request->type !== null && $request->type === 'on') {
            $type = 1;
        } 

        $newRequest = new Requests($request->all());
        $newRequest->document = $fileNameToStore;
        $newRequest->date = Carbon::parse($request->data);
        $newRequest->type = $type;
        $newRequest->save();

        return response()->json([
            'success' => true,
            'message' => 'Заявка успешно создана.'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $request = Requests::find($id);
        if(count($request) === 0) {
            return response()->json([
                'success' => false,
                'message' => 'Такой заявки не найдено.'
            ], 403);
        }

        $request->delete();
        
        return response()->json([
            'success' => true,
            'message' => 'Заявка успешно удалена.'
        ]);
    }

    /**
     * Download a specified request document.
     *
     * @param  file  $filename
     * @return \Illuminate\Http\Response
     */
    public function previewDoc($filename)
    {
        $filePath = public_path().'/crm/uploads/documents/'.$filename;
        $mimeType =  Storage::mimeType($filePath);
        
        return response()->file($filePath, [
            'Content-Type' => 'application/'.$mimeType
        ]);
    }

    /**
     * Download a specified request document.
     *
     * @param  file  $filename
     * @return \Illuminate\Http\Response
     */
    public function download($filename)
    {
        $filePath = public_path().'/crm/uploads/documents/'.$filename;
        // $mimeType =  Storage::mimeType($filePath);
        
        return response()->download($filePath);
    }

    /**
     * Display all the requests from database.
     *
     * @return \Illuminate\Http\Response
     */
    public function overview(Request $request)
    {
        $years = $this->getYears();      
        
        if(empty($request->query('y'))) 
            $year = Date('Y');
        else 
            $year = $request->query('y');  

        $requestsMonths = Requests::selectRaw('month(date) as month, year(date) as year, count(*) as count')
                                    ->whereRaw('year(date) = ?', $year)
                                    ->groupBy('year', 'month')
                                    ->orderByRaw('min(date) desc')
                                    ->get()->toArray();
        $allRequests = Requests::selectRaw('*, month(date) as month, year(date) as year')->get()->toArray();

        $temp = array();
        foreach($requestsMonths as $rm) {
            $temp[$rm['month']] = array();
            foreach($allRequests as $r) {
                if($rm['month'] === $r['month'] && $rm['year'] === $r['year']) {
                    array_push($temp[$rm['month']], [
                        'id' => $r['id'],
                        'client' => $r['client'],
                        'no' => $r['no'],
                        'date' => $r['date'],
                        'amount' => $r['amount'],
                        'route' => $r['route'],
                        'contact' => $r['contact'],
                        'document' => $r['document'],
                        'type' => $r['type']
                    ]);
                }                
            }
        }

        // return response()->json($temp);
        return view('crm.requests.overview')->with([ 'temp' => $temp, 'months' => $this->months, 'years' => $years ]);
    }

    /**
     * Helper method
     * Generate years for request filtering.
     *
     * @param  int $year
     * @return array $years
     */
    public function getYears()
    {
        $minYearArray = Requests::selectRaw('year(date) as year')->get()->toArray();

        $minYearTempArray = array();
        if(count($minYearTempArray) === 0) {
            $minYearTempArray[0] = date('Y');
        }

        foreach($minYearArray as $key => $value)
            array_push($minYearTempArray, $value['year']);

        $minYear = min($minYearTempArray);        
        $years = array();
        for($i = $minYear; $i < $minYear + 100; $i++)
            array_push($years, $i);

        return $years;
    }

    public function totalAmount(Request $request)
    {
        if($request->query('month') && $request->query('year')) {
            $month = $request->query('month');
            $year = $request->query('year');
        }

        $filteredRequests = Requests::select('*')->whereRaw('year(date) = ? and month(date) = ?', [ $year, $month ])->where('type', 0)->get();
        $totalAmount = 0;

        foreach ($filteredRequests as $key => $value) {
            $totalAmount += (int) $value['amount'];
        }

        return response()->json([
            'success' => true,
            'data' => $filteredRequests,
            'total' => $totalAmount,
            'month' => $this->months[$month - 1]
        ]);
    }
}
