<?php

namespace App\Http\Controllers\Crm;

use App\User;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Hash;
use Validator;
use Session;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:read-users');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('id', '<>', Auth::user()->id)->whereHas('roles', function($query) {
            $query->where('name', '<>', 'superadministrator');
        })->get();

        $roles = Role::all();

        return view('crm.users.index')->with([
            'users' => $users,
            'roles' => $roles
        ]);
    }

    public function apiGetUsers()
    {
        $user = User::with([ 'roles', 'permissions' ])->get();
        return response()->json($user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|unique:users|string',
            'password' => 'required|min:6|confirmed'
        ]);

        if($validator->fails()) {
            $errors = $validator->messages()->toJson();
            Session::flash('users.failed', $errors);
        }

        $user = new User($request->all());
        $user->password = bcrypt($request->password);
        $user->save();
        $user->attachRole((int) $request->role);

        Session::flash('user.created', 'Пользователь успешно создан.');
        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if(count($user) === 0) {
            return response()->json([
                'success' => false,
                'message' => 'Такого пользователя не существует. Попытка подмены данных.'
            ], 400);
        }

        $user->delete();

        return response()->json([
            'success' => true,
            'message' => 'Пользователь успешно удален.'
        ]);
    }

    /**
     * Change a password of a user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required|confirmed|min:6'
        ]);

        if($validator->fails()) {
            $errors = $validator->messages()->toJson();
            return response()->json([
                'success' => false,
                'message' => $errors
            ], 400);
        }

        $currentPassword = Auth::user()->password;
        $newPassword = $request->new_password;
        if(Hash::check($request->old_password, $currentPassword)) {
            $user = User::find(Auth::user()->id);
            $user->password = Hash::make($newPassword);
            $user->save();
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Старый пароль введен неверно.'
            ], 400);
        }

        return response()->json([
            'success' => true,
            'message' => 'Пароль был успешно изменен.'
        ]);
    }

    /**
     * Activate user account.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function changeStatus(Request $request)
    {
        $user = User::find($request->id);
        if(count($user) === 0) {
            return response()->json([
                'success' => false,
                'message' => 'Такого пользователя не существует. Попытка подмены данных.'
            ], 400);
        }

        $message = '';
        if((int) $request->status === 0) {
            $user->active = 0;
            $user->save();
            $message = 'Пользователь успешно деактивирован.';
        } else if((int) $request->status === 1) {
            $user->active = 1;
            $user->save();
            $message = 'Пользователь успешно активирован.';
        }

        return response()->json([
            'success' => true,
            'message' => $message
        ]);
    }
}
