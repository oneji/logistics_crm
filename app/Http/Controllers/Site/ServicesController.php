<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServicesController extends Controller
{
    public function index()
    {
        return view('site.pages.services.index');
    }

    public function overland()
    {
        return view('site.pages.services.overland');
    }

    public function airfreight() 
    {
        return view('site.pages.services.airfreight');
    }

    public function multimodal()
    {
        return view('site.pages.services.multimodal');
    }

    public function rails() 
    {
        return view('site.pages.services.rails');
    }

    public function downloadKP()
    {
        return response()->download(public_path('site/cube_kp.pdf'));
    }
}
