<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use Lang;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'username';
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/login');
    }

    public function login(Request $request) 
    {
        $credentials = [
            'username' => $request->username,
            'password' => $request->password
        ];

        $user = User::where('username', $credentials['username'])->where('active', 1);
        
        if(!$user->exists()) {
            Session::flash('user.notfound', 'Пользователь не найден.');
            return redirect()->route('login')->withInput([
                'username' => $request->only('username')
            ]);
        }   

        if (Auth::attempt($credentials)) {  
            return redirect()->route('crm.dashboard');
        }
        
        return redirect()->route('login')->withInput([
            'username' => $request->only('username')
        ])->withErrors([
            'password' => Lang::get('auth.password')
        ]);
    }
}
